//
//  ViewController.h
//  LSWeekView-Example
//
//  Created by Christoph Zelazowski on 6/16/14.
//  Copyright (c) 2014 Lumen Spark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACHViewControllerCalendar : UIViewController

@property (nonatomic, strong) NSString *perfomerID;
@property (nonatomic, strong) NSString *eventID;
@property (nonatomic, strong) NSDictionary *currentEventsList;
@property (nonatomic, strong) NSString *currentPerfomerName;
@property (nonatomic, strong) NSDictionary *fullPerformersList;

@end
