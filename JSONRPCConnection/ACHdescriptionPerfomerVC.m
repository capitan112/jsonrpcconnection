//
//  ACHdescriptionPerfomerVC.m
//  SimplyBook-me
//
//  Created by Капитан on 04.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHdescriptionPerfomerVC.h"
#import "ACHViewControllerCalendar.h"
#import "ACHDataSource.h"

static NSString *const descriptionToShedduleSegue = @"descriptionToShedduleSegue";

@interface ACHdescriptionPerfomerVC ()

@property (weak, nonatomic) IBOutlet UITextView *descriptionView;
@property (nonatomic, strong) ACHViewControllerCalendar *destinationController;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ACHdescriptionPerfomerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = self.fullPerformersList[@"name"];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    if (![self.fullPerformersList[@"description"] isEqual:[NSNull null]]) {
        self.descriptionView.text = self.fullPerformersList[@"description"];
    } else {
        self.descriptionView.text = @"";
    }
}

- (IBAction)selectedButton:(id)sender {
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
    if (isNetwork) {
        [self performSegueWithIdentifier:descriptionToShedduleSegue sender:self];
        _destinationController.perfomerID = self.perfomerID;
        _destinationController.eventID = self.eventID;
        _destinationController.currentEventsList = self.currentEventsList;
        _destinationController.currentPerfomerName = self.currentPerfomerName;
//        _destinationController.companyInfo = [self.companyInfo copy];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:descriptionToShedduleSegue]) {
        self.destinationController = [segue destinationViewController];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
