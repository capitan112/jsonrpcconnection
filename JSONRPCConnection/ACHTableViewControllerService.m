//
//  ACHTableViewControllerService.m
//  SimplyBookApp
//
//  Created by Капитан on 23.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHTableViewControllerService.h"
#import "ACHViewControllerCalendar.h"
#import "ACHDataSource.h"
#import "ACHTableViewControllerPerfomers.h"
#import "ACHdescriptionEventsVC.h"
#import "ACHCollectionViewSheddule.h"
#import "ACHGlobalData.h"
#import "ProgressHUD.h"
#import "ACHCompanyWebPageVC.h"

static NSString *const reuseCellIdentifier = @"сellService";
static NSString *const perfomerSegue = @"perfomerSegue";
static NSString *const descriptionEventSegue = @"descriptionEventSegue";
static NSString *const serviceSheduleSegue = @"serviceSheduleSegue";
static NSString *const webPageSegue = @"webPageSegue";

static const int rowHeight = 100;
static const int addititonalLabelX = 110;
static const int addititonalLabelY = 80;


@interface ACHTableViewControllerService ()

@property (nonatomic, strong) ACHDataSource *dataSource;
@property (nonatomic, strong) NSDictionary *allEventsList;
@property (nonatomic, strong) ACHTableViewControllerPerfomers *destinationController;
@property (nonatomic, strong) ACHdescriptionEventsVC *descriptionVC;
@property (nonatomic, strong) ACHCompanyWebPageVC *webPageVC;
@property (nonatomic, strong) NSArray *currentListOfEvents;
@property (nonatomic, strong) ACHCollectionViewSheddule *sheduleVC;
@property (nonatomic, strong) NSDictionary *fullPerformersList;
@property (nonatomic, strong) NSMutableString *serviceInfoText;

@end

@implementation ACHTableViewControllerService
@synthesize serviceInfoText;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Select a Service";
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _dataSource = [ACHDataSource sharedInstance];
    [self loadServices];
}

- (void)loadServices {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [_dataSource showProgressHUD];
        _allEventsList = [_dataSource getEventList];
        _fullPerformersList = [_dataSource getPerformersList];
//        NSLog(@"_allEventsList %@", _allEventsList);
        _currentListOfEvents = [_sendedListOfEvents copy];
        if ([_currentListOfEvents count] == 0 && [_allEventsList count] != 0) {
            _currentListOfEvents = [[_allEventsList allKeys] copy];
        }
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self.tableView reloadData];
            [_dataSource hideProgressHUD];
        });
    });
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_currentListOfEvents count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseCellIdentifier];
    }
    NSString *keyEvent = _currentListOfEvents[indexPath.row];
    cell.textLabel.text = _allEventsList[keyEvent][@"name"];
    cell.textLabel.numberOfLines = 1;
//    cell.textLabel.adjustsFontSizeToFitWidth = YES;
//    cell.textLabel.minimumScaleFactor = 0.5;
    NSString *serviceDescription = _allEventsList[keyEvent][@"description"];
    if ([serviceDescription isEqual:[NSNull null]]){
        serviceDescription = @"";
    }
//    cell.detailTextLabel.text = serviceDescription;
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    cell.detailTextLabel.numberOfLines = 3;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.imageView.image = [UIImage imageNamed:@"noPhotoCategory.png"];
    
    NSString *pictureName = _allEventsList[keyEvent][@"picture"];
    if (![pictureName isEqual:[NSNull null]]) {
        cell.imageView.backgroundColor = [UIColor whiteColor];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *image = [_dataSource pictureFormURL:_allEventsList[keyEvent][@"picture_path"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.imageView.image = image;
            });
        });
    }
    
    UILabel *serviceInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(addititonalLabelX, addititonalLabelY, 220.0, 15.0)];
    serviceInfoLabel.font = [UIFont systemFontOfSize:12.0];
    serviceInfoLabel.textColor = [self.view tintColor];
    
    NSString *duration = _allEventsList[keyEvent][@"duration"];
    int minutesDuration = [duration intValue];
    
    NSString *timeDuration;
    if (minutesDuration >= 60) {
        int hours = minutesDuration / 60;
        minutesDuration = minutesDuration - hours * 60;
        if (minutesDuration > 0) {
            timeDuration = [NSString stringWithFormat:@"%dh %dmin", hours, minutesDuration];
        } else {
            timeDuration = [NSString stringWithFormat:@"%dh", hours];
        }


    } else {
        timeDuration = [NSString stringWithFormat:@"%dmin", minutesDuration];
    }
    
    NSString *isHideDurationString = _allEventsList[keyEvent][@"hide_duration"];
    NSString *priceFromDict = _allEventsList[keyEvent][@"price"];
    serviceInfoText = [[NSMutableString alloc] init];
    NSString *priceString = nil;
    float priceFloat = 0;
    
    if (![priceFromDict isEqual:[NSNull null]]) {
        priceFloat = [priceFromDict floatValue];
        priceString = [NSString stringWithFormat:@"%.2f", priceFloat];
    }
    
    if (duration && [isHideDurationString isEqual:@"0"]) {
        [serviceInfoText appendFormat:@"%@ ", timeDuration];
    }
    
    if (priceString && priceFloat > 0) {
        NSString *currency = _allEventsList[keyEvent][@"currency"];
        [serviceInfoText appendFormat:@"%@ %@",currency, priceString];
    }
    
//    NSLog(@"serviceInfoText: %@",serviceInfoText);
    
    if (serviceInfoText) {
        serviceInfoLabel.text = serviceInfoText;
        [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [cell.contentView addSubview:serviceInfoLabel];
    }
    
    cell.detailTextLabel.text = serviceDescription;
    
    if (isCellBorder) {
        cell.imageView.layer.borderWidth = 0.5f;
        cell.imageView.layer.MasksToBounds = YES;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
    if (isNetwork) {
        BOOL isAnyUnitPlugin = [_dataSource isPluginActivated:@"any_unit"];
        BOOL isHideUnits = NO;
        NSDictionary *anyUnitData = [_dataSource getAnyUnitData];
        
        NSString *keyEvent = _currentListOfEvents[indexPath.row];
        NSString *numberString = _allEventsList[keyEvent][@"price"];
        if (![numberString isEqual:[NSNull null]]){
            NSString *eventID = _allEventsList[keyEvent][@"id"];
            BOOL isPaymentRequired = [self.dataSource isPaymentRequired:eventID];
            if (isPaymentRequired) {
                [self performSegueWithIdentifier:webPageSegue sender:self];
                NSString *websiteString = [NSString stringWithFormat:@"https://%@.simplybook.me", [self.dataSource companyLogin]];
//                NSLog(@"%@", websiteString);
                self.webPageVC.fullURL = [websiteString copy];
                return;
            }
        }
        
        if (![anyUnitData isEqual:[NSNull null]]) {
            isHideUnits = [anyUnitData[@"hide_other_units"] boolValue];
        }
        
//        NSLog(@"isAnyUnitPlugin %hhd isHideUnits %hhd", isAnyUnitPlugin, isHideUnits);
        if (isAnyUnitPlugin && isHideUnits) {
            [self performSegueWithIdentifier:serviceSheduleSegue sender:self];
            NSString *eventID = _currentListOfEvents[indexPath.row];
            self.sheduleVC.eventID = eventID;
            self.sheduleVC.perfomerID = @"";
            self.sheduleVC.currentPerfomerName = @"Any perfomer";
            self.sheduleVC.currentEventsList = _allEventsList[_currentListOfEvents[indexPath.row]];
            self.sheduleVC.fullPerformersList = _fullPerformersList;
        } else {
            NSString *keyEvents = _currentListOfEvents[indexPath.row];
            NSArray *currentPerfomersList = [[_allEventsList[keyEvents][@"unit_map"]allKeys] copy];
//            NSLog(@"_allEventsList %@", _allEventsList);
//            NSLog(@"currentPerfomersList %@", currentPerfomersList);
            if ([currentPerfomersList count] == 1 || !currentPerfomersList) {
//            if ([currentPerfomersList count] == 1) {
                if (!currentPerfomersList) {
                    currentPerfomersList = [[_fullPerformersList allKeys] copy];
                }
                
                [self performSegueWithIdentifier:serviceSheduleSegue sender:self];
                NSString *eventID = _currentListOfEvents[indexPath.row];
                self.sheduleVC.eventID = eventID;
                NSString *pefromerID = currentPerfomersList[0];
                self.sheduleVC.perfomerID = pefromerID;
                self.sheduleVC.currentPerfomerName = _fullPerformersList[pefromerID][@"name"];
                self.sheduleVC.currentEventsList = _allEventsList[_currentListOfEvents[indexPath.row]];
                self.sheduleVC.fullPerformersList = _fullPerformersList;
            } else {
                [self performSegueWithIdentifier:perfomerSegue sender:self];
                _destinationController.sendedPerfomers = currentPerfomersList;
                _destinationController.eventID = _currentListOfEvents[indexPath.row];
                _destinationController.currentEventsList = _allEventsList[_currentListOfEvents[indexPath.row]];
                _destinationController.fullPerformersList = _fullPerformersList;
            }
        }
    }
}

#pragma mark - Accessory button

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
    if (isNetwork) {
        [self performSegueWithIdentifier:descriptionEventSegue sender:self];
        NSString *keyEvents = _currentListOfEvents[indexPath.row];
        self.descriptionVC.allEventsList = [_allEventsList[keyEvents] copy];
        NSArray *currentPerfomersList = [_allEventsList[keyEvents][@"unit_map"]allKeys];
        self.descriptionVC.sendedPerfomers = currentPerfomersList;
        self.descriptionVC.eventID = _currentListOfEvents[indexPath.row];
        self.descriptionVC.currentEventsList = _allEventsList[_currentListOfEvents[indexPath.row]];
//        NSLog(@"serviceInfoText %@", serviceInfoText);
        
        self.descriptionVC.serviceInfoText = [serviceInfoText mutableCopy];
        self.descriptionVC.fullPerformersList = [_fullPerformersList copy];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:perfomerSegue]) {
        _destinationController = [segue destinationViewController];
//        _destinationController.companyInfo = [self.companyInfo copy];
        
    }
    
    if ([segue.identifier isEqualToString:descriptionEventSegue]) {
        self.descriptionVC = [segue destinationViewController];
    }
    
    if ([segue.identifier isEqualToString:serviceSheduleSegue]) {
        self.sheduleVC = [segue destinationViewController];
    }

    if ([segue.identifier isEqualToString:webPageSegue]) {
        self.webPageVC = [segue destinationViewController];
    }
    


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
