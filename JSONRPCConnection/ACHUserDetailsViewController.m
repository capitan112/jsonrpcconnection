//
//  ACHUserDetailsViewController.m
//  SimplyBookApp
//
//  Created by Капитан on 02.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#define kEmailTextFieldTag 10
#define kPhoneTextFieldTag 20


#import "ACHUserDetailsViewController.h"
#import "HMDiallingCode.h"
#import "ACHViewControllerReservation.h"

static NSString *const userDetailsKey = @"userDetailKey";
static NSString *const userDetailReservationSegue = @"userDetailReservationSegue";

@interface ACHUserDetailsViewController() <HMDiallingCodeDelegate>

@property (strong, nonatomic) NSMutableDictionary *userDetails;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (strong, nonatomic) HMDiallingCode *diallingCode;
@property (strong, nonatomic) NSString *phoneCodeArea;
@property (strong, nonatomic)  ACHViewControllerReservation *reservationController;

@end

@implementation ACHUserDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *dictionaryData = [defaults objectForKey:userDetailsKey];
    
    if (!_userDetails){
        _userDetails = [[NSMutableDictionary alloc] init];
    }
    if (dictionaryData) {
        _userDetails = [[NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData] mutableCopy];

    }
    _nameTextField.text = _userDetails[@"name"];
    _emailTextField.text = _userDetails[@"email"];
    _phoneTextField.text = _userDetails[@"phone"];
    [self addTapGesture];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    UIToolbar *numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    self.phoneTextField.inputAccessoryView = numberToolbar;

    self.confirmButton.layer.cornerRadius = 5;
    self.confirmButton.layer.borderWidth = 1;
    self.confirmButton.layer.borderColor = self.confirmButton.tintColor.CGColor;
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey:NSLocaleCountryCode];
    self.diallingCode = [[HMDiallingCode alloc] initWithDelegate:self];
    [self.diallingCode getDiallingCodeForCountry:countryCode];
    self.navigationItem.hidesBackButton = YES;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([_phoneTextField.text isEqualToString:@""]) {
        _phoneTextField.text = self.phoneCodeArea;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_userDetails setObject:_nameTextField.text forKey:@"name"];
    [_userDetails setObject:_emailTextField.text forKey:@"email"];
    [_userDetails setObject:_phoneTextField.text forKey:@"phone"];
    [self.delegate userDetailsDictionary:[_userDetails copy]];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_userDetails];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:userDetailsKey];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - textFields delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

-(void)cancelNumberPad {
    [self.phoneTextField resignFirstResponder];
}

-(void)doneWithNumberPad {
    [self.phoneTextField resignFirstResponder];
}

- (UIAlertView *)alertWithTitle:(NSString*)title message:(NSString*)message {
    UIAlertView *alert =[[UIAlertView alloc ] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles: nil];
    return alert;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tap geste

- (void)addTapGesture {
    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = FALSE;
    [self.view addGestureRecognizer:tapper];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

#pragma mark - textField validation

- (BOOL)textFieldsValidation {
    if(![self validateEmail:self.emailTextField.text]) {
        UIAlertView *alert = [self alertWithTitle:@"Mail validation" message:@"Not valid mail"];
        [alert show];
        return NO;
    }

    if(![self validatePhone:self.phoneTextField.text]) {
        UIAlertView *alert = [self alertWithTitle:@"Phone validation" message:@"Not valid phone number"];
        [alert show];
        return NO;
    }
    
    return YES;
}

- (BOOL)validateEmail:(NSString *)candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate] ;
}

- (BOOL)validatePhone:(NSString *)candidate {
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:candidate];
}

#pragma mark - textField lift View

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField.tag == kEmailTextFieldTag) {
        [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardDidShowNotification object:self];

    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (textField.tag == kEmailTextFieldTag) {
        [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardDidHideNotification object:self];
    }
    [self.view endEditing:YES];

    return YES;
}

#pragma mark - keyBoard show/hide

- (void)keyboardDidShow:(NSNotification *)notification {
    [self.view setFrame:CGRectMake(0, -20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
}

-(void)keyboardDidHide:(NSNotification *)notification {
    [self.view setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
}

#pragma mark - button confirm

- (IBAction)confirmButton:(id)sender {
//    NSLog(@"self.dataReservation %@", self.dataReservation);
    if (self.dataReservation) {
        [self performSegueWithIdentifier:userDetailReservationSegue sender:nil];
        self.reservationController.dataReservation = [self.dataReservation mutableCopy];
        self.reservationController.fullPerformersList = [self.fullPerformersList copy];
        self.reservationController.userDetails = self.userDetails;
//        NSLog(@"fullPerformersList %@", self.fullPerformersList);
    } else {
        BOOL isValid = [self textFieldsValidation];
        if (isValid)
            [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - HMDiallingCodeDelegate

- (void)didGetDiallingCode:(NSString *)diallingCode forCountry:(NSString *)countryCode {
    self.phoneCodeArea = [NSString stringWithFormat:@"+%@", diallingCode];
}

- (void)failedToGetDiallingCode {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Whoops! Something went wrong." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
}


#pragma mark - segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:userDetailReservationSegue]) {
        self.reservationController = segue.destinationViewController;
    }
}

@end
