//
//  Booking.m
//  SimplyBook-me
//
//  Created by Капитан on 23.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "Booking.h"
#import "Company.h"


@implementation Booking

@dynamic additionalFields;
@dynamic bookingHash;
@dynamic bookingId;
@dynamic code;
@dynamic perfomerName;
@dynamic serviceName;
@dynamic startDateTime;
@dynamic companyName;

@end
