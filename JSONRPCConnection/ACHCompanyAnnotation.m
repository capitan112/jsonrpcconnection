    //
//  ACHCompanyAnnotation.m
//  SimplyBook-me
//
//  Created by Капитан on 26.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHCompanyAnnotation.h"

NSString *const kReusablePinRed = @"Red";
NSString *const kReusablePinGreen = @"Green";
NSString *const kReusablePinPurple = @"Purple";

@implementation ACHCompanyAnnotation


+ (NSString *) reusableIdentifierforPinColor:(MKPinAnnotationColor)paramColor {
    NSString *result = nil;
    switch (paramColor){
    case MKPinAnnotationColorRed: {
        result = kReusablePinRed;
        break;
    }
    case MKPinAnnotationColorGreen: {
        result = kReusablePinGreen;
        break;
    }
    case MKPinAnnotationColorPurple: {
        result = kReusablePinPurple;
        break;
    }
}
    return result;
}

- (instancetype)initWithCoordinates:(CLLocationCoordinate2D)paramCoordinates title:(NSString*)paramTitle subTitle:(NSString *)paramSubTitle login:(NSString *)login {
    self = [super init];

    if (self != nil) {
        _coordinate = paramCoordinates;
        _title = paramTitle;
        _subtitle = paramSubTitle;
        _login = login;
        _pinColor = MKPinAnnotationColorGreen;
    }
    
    return self;
}

@end
