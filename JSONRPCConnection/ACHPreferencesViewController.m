//
//  ACHPreferencesViewController.m
//  SimplyBook-me
//
//  Created by Капитан on 27.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHPreferencesViewController.h"


static NSString *const preferencesUserDetailSegue = @"preferencesUserDetailSegue";
static NSString *const preferencesCell = @"preferencesCell";
const float kExponet = 2.3;

@interface ACHPreferencesViewController ()

@property (weak, nonatomic) IBOutlet UISlider *distanseSlider;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (nonatomic, strong) NSUserDefaults *defaults;
@property (nonatomic, strong) NSDictionary *userDetails;
@property (nonatomic, strong) ACHUserDetailsViewController *userDetailController;
@property (nonatomic) int seachingDistance;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ACHPreferencesViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.defaults = [NSUserDefaults standardUserDefaults];
    int sliderValue = (int)[[self.defaults objectForKey:@"sliderValue"] integerValue];

    if (sliderValue <= 1) {
        sliderValue = 3;
    }
    
    [self.distanseSlider setValue:sliderValue animated:NO];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setDistanceLabel];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    NSLog(@"self.seachingDistance %d", self.seachingDistance);
    [_defaults setValue:[NSNumber numberWithInt:self.seachingDistance]  forKey:@"seachingDistance"];
    [_defaults setValue:[NSNumber numberWithInt:(int)_distanseSlider.value]  forKey:@"sliderValue"];

    
    [_defaults synchronize];
    
}

- (void)setDistanceLabel {
    self.seachingDistance =  (int) powf(kExponet , (int)_distanseSlider.value);
    self.distanceLabel.text = [NSString stringWithFormat:@"%d km", self.seachingDistance];
}


- (IBAction)changedDistance:(UISlider *)sender {
    [self setDistanceLabel];
//   self.distanceLabel.text = [NSString stringWithFormat:@"%d km", (int)powf(kExponet,(int)[sender value])];
//    NSLog(@"distanceLabel.text %@ slider value %d", self.distanceLabel.text, (int)_distanseSlider.value);
//    NSLog(@"self.seachingDistance %d", self.seachingDistance);
}

- (void)userDetailsDictionary:(NSDictionary *)dict {
    _userDetails = [dict mutableCopy];
}

- (IBAction)pressedUserDatails:(id)sender {
    [self performSegueWithIdentifier:preferencesUserDetailSegue sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:preferencesUserDetailSegue]) {
        self.userDetailController = [segue destinationViewController];
        self.userDetailController.delegate = self;
    }
}

#pragma mark Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:preferencesCell];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:preferencesCell];
    }
    if (indexPath.row == 0) {
        cell.textLabel.text = @"User detail";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:preferencesUserDetailSegue sender:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
