//
//  ACHFavoritesTableViewController.m
//  SimplyBook-me
//
//  Created by Капитан on 21.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHFavoritesTableViewController.h"
#import "ACHAppDelegate.h"
#import "Booking.h"
#import "Company.h"
#import "ACHCompanyInfoViewController.h"
#import "ACHSeachListTableViewController.h"
#import "ACHDataSource.h"
#import "ACHGlobalData.h"

static NSString *const reuseCellIdentifier = @"favoritesCell";
static NSString *const companyInfoSegue = @"favoritesInfoSegue";
static NSString *const directInfoSegue = @"directInfoSegue";
static NSString *const infoSegue = @"infoSegue";
static const int rowHeight = 110;

@interface ACHFavoritesTableViewController () <UIActionSheetDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) ACHAppDelegate *application;

@end

@implementation ACHFavoritesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadManagedObjectContext];

    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    NSError *error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        NSLog(@"Error! %@",error);
        abort();
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor]};

}

#pragma mark - Fetch result controller

- (void)loadManagedObjectContext {
    self.application = (ACHAppDelegate *) [[UIApplication sharedApplication]delegate];
    self.managedObjectContext = self.application.managedObjectContext;
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
//    [self loadManagedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Company"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"companyName"
                                                                   ascending:YES];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"favorites = %hhd", YES]];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest: fetchRequest
                                 managedObjectContext: self.managedObjectContext
                                   sectionNameKeyPath: nil
                                            cacheName: nil];

    _fetchedResultsController.delegate = self;
//    NSLog(@"_fetchedResultsController %@", _fetchedResultsController);
    
    return _fetchedResultsController;
}

#pragma mark Fetched Results delegate

- (void) controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void) controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;

    switch (type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
//        {
//            Company *companyForUpgrade = [self.fetchedResultsController objectAtIndexPath:indexPath];
//            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//            cell.textLabel.text = companyForUpgrade.companyName;
//            NSLog(@"companyForUpgrade.companyName %@", companyForUpgrade.companyName);
//            
//            NSString *companyDescriptionInfo = [NSKeyedUnarchiver unarchiveObjectWithData:companyForUpgrade.companyDescription];
//            if (![companyDescriptionInfo isEqual:[NSNull null]]) {
//                cell.detailTextLabel.text = companyDescriptionInfo;
//            }
//            if (![companyForUpgrade.logo isEqual:[NSNull null]] || companyForUpgrade.logo != nil) {
//                UIImage *companyLogo = [NSKeyedUnarchiver unarchiveObjectWithData:companyForUpgrade.logo];
//                cell.imageView.image = companyLogo;
//                }
            
//            }
            [self.tableView reloadData];
            break;

            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;

            
    }
    
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;

    }
}

#pragma mark - Table view data source

- (void)refreshData {
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> secInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [secInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseCellIdentifier];
    }
    
    Company *company = [self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.row];
    cell.detailTextLabel.numberOfLines = 4;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = company.companyName;

    NSString *companyDescriptionInfo = [NSKeyedUnarchiver unarchiveObjectWithData:company.companyDescription];
    if (![companyDescriptionInfo isEqual:[NSNull null]]) {
        cell.detailTextLabel.text = companyDescriptionInfo;
    }

    cell.imageView.image = [UIImage imageNamed:@"noPhotoCategory.png"];
    if (![company.logo isEqual:[NSNull null]] && company.logo) {
        UIImage *companyLogo = [NSKeyedUnarchiver unarchiveObjectWithData:company.logo];
        cell.imageView.image = companyLogo;
    }
    
    if (isCellBorder) {
        cell.imageView.layer.borderWidth = 0.5f;
        cell.imageView.layer.MasksToBounds = YES;
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        [self performSegueWithIdentifier:directInfoSegue sender:self];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self managedObjectContext];
        Company *company = [self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.row];
        company.favorites = NO;
        
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Error! %@",error);
        }
    }
}

#pragma mark - Segue Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:directInfoSegue]) {
        ACHDataSource *dataSource = [ACHDataSource sharedInstance];
        [dataSource showProgressHUD];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Company *company = [self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.row];
        ACHCompanyInfoViewController *destinationController =  [segue destinationViewController];
        destinationController.companyLogin = company.login;
    
    }
}

#pragma mark activityButton

- (IBAction)activityButtonTouched:(id)sender {
    NSString *textToShare = kMessageForShare;
    NSURL *website = [NSURL URLWithString:@"https://simplybook.me"];
    NSArray *objectsToShare = @[textToShare, website];
    
    UIActivityViewController *activity = [[UIActivityViewController alloc]
                                          initWithActivityItems:objectsToShare
                                          applicationActivities:nil];
    [self presentViewController:activity animated:YES completion:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
