//
//  ACHCompanyAnnotation.h
//  SimplyBook-me
//
//  Created by Капитан on 26.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

extern NSString *const kReusablePinRed;
extern NSString *const kReusablePinGreen;
extern NSString *const kReusablePinPurple;

@interface ACHCompanyAnnotation : NSObject<MKAnnotation>

@property (nonatomic, unsafe_unretained, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *login;
@property (nonatomic, unsafe_unretained) MKPinAnnotationColor pinColor;

- (instancetype)initWithCoordinates:(CLLocationCoordinate2D)paramCoordinates title:(NSString*)paramTitle subTitle:(NSString*)paramSubTitle login:(NSString *)login;

+ (NSString *)reusableIdentifierforPinColor :(MKPinAnnotationColor)paramColor;

@end
