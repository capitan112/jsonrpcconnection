
#import "ACHTableViewControllerCategory.h"
#import "ACHDataSource.h"
#import "ACHTableViewControllerService.h"
#import "ACHDescriptionDetailViewController.h"
#import "ACHGlobalData.h"

static NSString *const reuseCellIdentifier = @"сellCategory";
static NSString *const serviceCategory = @"event_category";
static NSString *const descriptionDetailSegue = @"descriptionDetailSegue";
static NSString *const serviceSegue = @"serviceSegue";

static const int rowHeight = 110;

@interface ACHTableViewControllerCategory ()

@property (nonatomic, strong) ACHDataSource *dataSource;
@property (nonatomic, strong) ACHTableViewControllerService *destinationController;
@property (nonatomic, strong) ACHDescriptionDetailViewController *descriptionController;
@property (nonatomic, strong) UIActivityIndicatorView *activityView;
@property (nonatomic, assign) BOOL isCategoryExist;

@end

@implementation ACHTableViewControllerCategory

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Select a Category";
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.dataSource = [ACHDataSource sharedInstance];
    [_dataSource hideProgressHUD];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_allCategory count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseCellIdentifier];
    }
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    cell.textLabel.minimumScaleFactor = 0.6;
    cell.textLabel.text = _allCategory[indexPath.row][@"name"];

    NSString *companyDescription = _allCategory[indexPath.row][@"description"];
    
    if ([companyDescription isEqual:[NSNull null]]){
        companyDescription = @"";
    }
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    cell.detailTextLabel.text = companyDescription;
    cell.textLabel.numberOfLines = 2;
    cell.detailTextLabel.numberOfLines = 4;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.imageView.image = [UIImage imageNamed:@"noPhotoCategory.png"];
  
    NSString *pictureName = _allCategory[indexPath.row][@"picture"];
    if (![pictureName isEqual:[NSNull null]]) {
        cell.imageView.backgroundColor = [UIColor whiteColor];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *image = [_dataSource pictureFormURL:_allCategory[indexPath.row][@"picture_path"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.imageView.image = image;
            });
        });
    }
    
    if (isCellBorder) {
        cell.imageView.layer.borderWidth = 0.5f;
        cell.imageView.layer.MasksToBounds = YES;
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
    if (isNetwork) {
        [self performSegueWithIdentifier:serviceSegue sender:self];
        _destinationController.sendedListOfEvents = _allCategory[indexPath.row][@"events"];
//        _destinationController.companyInfo = [self.companyInfo copy];
    }
}

- (void)tableView:(UITableView *)tableView
accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
    if (isNetwork) {
        [self performSegueWithIdentifier:descriptionDetailSegue sender:self];
        self.descriptionController.allCategory = [_allCategory[indexPath.row] copy];
        self.descriptionController.sendedListOfEvents = _allCategory[indexPath.row][@"events"];
//        self.descriptionController.companyInfo = [self.companyInfo copy];
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:serviceSegue]) {
        _destinationController = [segue destinationViewController];
    }
    if ([segue.identifier isEqualToString:descriptionDetailSegue]) {
        self.descriptionController = [segue destinationViewController];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
