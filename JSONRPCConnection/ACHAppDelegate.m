//
//  CAAppDelegate.m
//  JSONRPCConnection
//
//  Created by Капитан on 04.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHAppDelegate.h"
#import "ACHDataSource.h"
#import "Booking.h"
#import "Company.h"
#import "ACHCompanyInfoViewController.h"
#import "ACHFavoritesTableViewController.h"
#import "ACHHistoryDetailViewController.h"
#import "ACHHistoryTableViewController.h"

@interface ACHAppDelegate ()

@property (strong, nonatomic) NSMutableArray *controllers;

@end


@implementation ACHAppDelegate
@synthesize controllers;


#pragma mark - APNS notification methods

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    self.deviceToken = [[[deviceToken description]
                        stringByTrimmingCharactersInSet:[NSCharacterSet
                     characterSetWithCharactersInString:@"<>"]]
                   stringByReplacingOccurrencesOfString:@" "
                                             withString:@""];
    NSLog(@"Did Register for Remote Notifications with Device Token (%@)", self.deviceToken);
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Fail to Register for Remote Notifications");
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    NSLog(@"Push message: %@", userInfo);
    
    if([application applicationState] == UIApplicationStateActive) {

//        [self addToDateBase:userInfo];

        NSString *message = userInfo[@"aps"][@"alert"];
        UIAlertView *alert =[[UIAlertView alloc ] initWithTitle:@"Notification:"
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    } else {
        [self generateVC:userInfo];
        self.tabBarController.selectedIndex = 2;
        self.tabBarController.viewControllers = controllers;
        self.window.rootViewController = self.tabBarController;
        
    }
}


#pragma mark - appDelegete methods

- (NSFetchRequest *)addToDateBase:(NSDictionary *)dict {
    NSString *bookingHash = dict[@"booking_hash"];
    NSFetchRequest *requestForBooking = [NSFetchRequest fetchRequestWithEntityName:@"Booking"];
    [requestForBooking setPredicate:[NSPredicate predicateWithFormat:@"bookingHash = %@", bookingHash]];
    [requestForBooking setFetchLimit:1];
    
    NSError *fetchErrorBook;
    
    NSUInteger count = [self.managedObjectContext countForFetchRequest:requestForBooking error:&fetchErrorBook];
    
    if (count == NSNotFound) {
        NSLog(@"Error %@", fetchErrorBook);
    } else if (count == 0) {
        
        NSString *bookingId = dict[@"booking_id"];
        NSString *companyLogin = dict[@"company"];
        [ACHDataSource selfDestruction];
        ACHDataSource *dataSource = [ACHDataSource sharedInstanceWithLogin:companyLogin];
        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            [dataSource showProgressHUD];
//        });

        NSDictionary *bookingInfo = [dataSource bookingInfoWithID:bookingId bookingHash:bookingHash];
        //        NSLog(@"bookingInfo %@", bookingInfo);
        [ACHDataSource selfDestruction];
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
        [request setPredicate:[NSPredicate predicateWithFormat:@"login = %@", companyLogin]];
        [request setFetchLimit:1];
        
        NSError *fetchErrorCompany;
        Company *company;
        NSUInteger count = [self.managedObjectContext countForFetchRequest:request error:&fetchErrorCompany];
        if (count == NSNotFound) {
            NSLog(@"Error %@", fetchErrorCompany);
        } else if (count == 0) {
            company = (Company *) [NSEntityDescription insertNewObjectForEntityForName:@"Company"
                                                                inManagedObjectContext:
                                   self.managedObjectContext];
            [company setLogin:companyLogin];
        } else {
            company = [[self.managedObjectContext executeFetchRequest:request error:&fetchErrorCompany] objectAtIndex:0];
        }
        
        Booking *history = (Booking *)[NSEntityDescription insertNewObjectForEntityForName:@"Booking" inManagedObjectContext:self.managedObjectContext];
        [history setPerfomerName:bookingInfo[@"unit_name"]];
        [history setServiceName:bookingInfo[@"event_name"]];
        [history setCode:bookingInfo[@"code"]];
        [history setBookingHash:bookingHash];
        
        NSString *reservedDate = [NSString stringWithFormat:@"%@", bookingInfo[@"start_date_time"]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [history setStartDateTime:[dateFormatter dateFromString:reservedDate]];
        
        NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc] init];
        [numberFormat setNumberStyle:NSNumberFormatterDecimalStyle];
        [history setBookingId:[numberFormat numberFromString:bookingInfo[@"id"]]];
        NSArray *additionalFieldsArray = bookingInfo[@"additional_fields"];
        //        NSLog(@"additionalFieldsArray %d", [additionalFieldsArray count]);
        NSMutableDictionary *additionalFieldsDict = [[NSMutableDictionary alloc] init];
        for (NSDictionary *item in additionalFieldsArray) {
            [additionalFieldsDict setValue:item[@"value"] forKey:item[@"field_title"]];
        }
        NSData *additionalFieldsData = [NSKeyedArchiver archivedDataWithRootObject:additionalFieldsDict];
        [history setAdditionalFields:additionalFieldsData];
        [company addBookingObject:history];
        
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Error! %@", error);
        }
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            [dataSource hideProgressHUD];
//        });
        
    }
    return requestForBooking;
}

- (void)generateVC:(NSDictionary *)dict {
    ACHDataSource *dataSource = [ACHDataSource sharedInstance];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [dataSource showProgressHUD];
    });
    
    UINavigationController *historyNVC = [self.storyboard instantiateViewControllerWithIdentifier:@"historyNVC"];
    ACHHistoryDetailViewController *historyDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"historyDetailVC"];
    
    NSFetchRequest *requestForBooking;
    Booking *selectedHistory;
    requestForBooking = [self addToDateBase:dict];

    NSError *error;
    NSArray *array = [self.managedObjectContext executeFetchRequest:requestForBooking error:&error];
    if ([array count] > 0) {
        selectedHistory = array[0];
        historyDetailVC.selectedHistory = selectedHistory;
        [historyNVC pushViewController:historyDetailVC animated:NO];
        controllers[2] = historyNVC;
        self.tabBarController.selectedIndex = 2;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [dataSource hideProgressHUD];
    });
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    ACHDataSource *dataSource = [ACHDataSource sharedInstance];
    dispatch_async(dispatch_get_main_queue(), ^{
        [dataSource showProgressHUD];
    });

    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge|UIUserNotificationTypeAlert|UIUserNotificationTypeSound
                                                                                 categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         UIRemoteNotificationTypeBadge |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeSound];
    }
    
    self.storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.tabBarController = [self.storyboard instantiateViewControllerWithIdentifier:@"MainTabBarVC"];
    UINavigationController *searchNVC = [self.storyboard instantiateViewControllerWithIdentifier:@"searchNVC"];
    UINavigationController *favoritesNVC = [self.storyboard instantiateViewControllerWithIdentifier:@"favoriteNVC"];
    UINavigationController *historyNVC = [self.storyboard instantiateViewControllerWithIdentifier:@"historyNVC"];
    UINavigationController *preferencesNVC = [self.storyboard instantiateViewControllerWithIdentifier:@"preferencesNVC"];
    controllers = [NSMutableArray arrayWithObjects:searchNVC, favoritesNVC, historyNVC, preferencesNVC, nil];
    NSDictionary *remoteNotif = [launchOptions objectForKey:
                                        UIApplicationLaunchOptionsRemoteNotificationKey];
    if(remoteNotif) {
        [self generateVC:remoteNotif];
    }
    self.tabBarController.viewControllers = controllers;
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [dataSource hideProgressHUD];
    });
//    [self resetApplicationModel];
//    [self fetchObjects];
    
    return YES;
}

- (void)resetApplicationModel {
//    NSError *error;
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"SimplyBookingHistory.sqlite"];
    [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
    for (NSManagedObject *ct in [self.managedObjectContext registeredObjects]) {
        [self.managedObjectContext deleteObject:ct];
    }
    NSLog(@"Core data deleted");
    
    //Make new persistent store for future saves
//    if (![self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
//        // do something with the error
//    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self saveContext];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [self saveContext];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];
}

#pragma mark - URL scheme

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    NSString *companyLogin = [[url.absoluteString componentsSeparatedByString:@"://"] lastObject];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"login = %@", companyLogin]];
    [request setFetchLimit:1];
//    NSLog(@"companyLogin %@", companyLogin);
    Company *company;
    NSError *fetchError;
    NSUInteger count = [self.managedObjectContext countForFetchRequest:request error:&fetchError];

    if (count == NSNotFound) {
        NSLog(@"Error %@", fetchError);
    } else if (count == 0) {
        [ACHDataSource selfDestruction];
        ACHDataSource *dataSource = [ACHDataSource sharedInstanceWithLogin:companyLogin];
        NSDictionary *companyInfo = [dataSource getCompanyInfo];
//        NSLog(@"companyInfo %@", companyInfo);
        if (companyInfo) {
            NSString *companyName = companyInfo[@"name"];
            company = (Company *)[NSEntityDescription insertNewObjectForEntityForName:@"Company"
                                                                inManagedObjectContext:self.managedObjectContext];
            [company setLogin:companyLogin];
            [company setCompanyName:companyName];
            [company setFavorites:YES];
            [company setAddressOne:companyInfo[@"address1"]];
            [company setAddressTwo:companyInfo[@"address2"]];
            [company setCity:companyInfo[@"city"]];
            [company setEmail:companyInfo[@"email"]];
            [company setPhone:companyInfo[@"phone"]];
            NSString *companyDescriptionInfo = companyInfo[@"description_text"];
    //        NSString *textLabel = [self parseDescriptionInfo:companyDescriptionInfo];
            NSData *descriptionData = [NSKeyedArchiver archivedDataWithRootObject:companyDescriptionInfo];
            [company setCompanyDescription:descriptionData];
            if (companyInfo[@"logo"]) {
                UIImage *image = [dataSource pictureFormURL:companyInfo[@"logo"]];
                NSData *logoImageData = [NSKeyedArchiver archivedDataWithRootObject:image];
                [company setLogo:logoImageData];
                NSLog(@"image loaded");
            }
            [self saveContext];
        }
    } else {
        Company *companyForUpgrade = [[self.managedObjectContext executeFetchRequest:request error:&fetchError] objectAtIndex:0];
        if (!companyForUpgrade.favorites){
            [companyForUpgrade setFavorites:YES];
            [self saveContext];
        }
//        NSLog(@"favorites %hhd", companyForUpgrade.favorites);
    }
    UINavigationController *favoritesNVC = [self.storyboard instantiateViewControllerWithIdentifier:@"favoriteNVC"];
    ACHCompanyInfoViewController *infoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"companyInfoVC"];
    infoVC.companyLogin = companyLogin;
    [favoritesNVC pushViewController:infoVC animated:YES];
    controllers[1] = favoritesNVC;
//    [controllers replaceObjectAtInxdex:1 withObject:favoritesNVC];
    self.tabBarController.viewControllers = controllers;
    self.window.rootViewController = self.tabBarController;
    self.tabBarController.selectedIndex = 1;

    return YES;
}


#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
//    NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory  inDomains:NSUserDomainMask] lastObject]);

    // The directory the application uses to store the Core Data store file. This code uses a directory named "Capitan.CoreDataTest" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"SimplyBookingHistory" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    // Create the coordinator and store
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"SimplyBookingHistory.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)fetchObjectsWitnLogin:(NSString *)login {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Company" inManagedObjectContext:self.managedObjectContext];
    fetchRequest.resultType = NSDictionaryResultType;
    fetchRequest.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"bookingHash"]];
    fetchRequest.returnsDistinctResults = YES;

    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"login", login]];
    NSArray *dictionaries = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
//    NSLog (@"start_date_time: %@",dictionaries);
}

- (void)deleteObjects {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    
    [fetch setEntity:[NSEntityDescription entityForName:@"BookingHistory" inManagedObjectContext:context]];
    NSArray *result = [context executeFetchRequest:fetch error:nil];
    for (id basket in result)
        [context deleteObject:basket];
    [self saveContext];
}

#pragma mark - State Restoration

//- (BOOL)application:(UIApplication *)application shouldSaveApplicationState:(NSCoder *)coder {
//    return YES;
//}
//
//- (BOOL)application:(UIApplication *)application shouldRestoreApplicationState:(NSCoder *)coder {
//    return YES;
//}
//
//- (UIViewController *)application:(UIApplication *)application
//viewControllerWithRestorationIdentifierPath:(NSArray *)identifierComponents
//                                      coder:(NSCoder *)coder {
//    UIViewController *vc = [[UINavigationController alloc] init];
//    vc.restorationIdentifier = [identifierComponents lastObject];
//    if ([identifierComponents count] == 1) {
//        self.window.rootViewController = vc;
//    }
//    
//    return vc;
//}

@end
