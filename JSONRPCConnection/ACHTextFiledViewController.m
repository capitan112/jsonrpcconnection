//
//  ACHTextFiledViewController.m
//  SimplyBook-me
//
//  Created by Капитан on 09.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHTextFiledViewController.h"

@interface ACHTextFiledViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@end

@implementation ACHTextFiledViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textField.text = self.selectedValue;
    if ([self.typeOfFields isEqualToString:@"digits"]) {
         [self.textField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    }
    self.confirmButton.layer.cornerRadius = 5;
    self.confirmButton.layer.borderWidth = 1;
    self.confirmButton.layer.borderColor = self.confirmButton.tintColor.CGColor;
    self.navigationItem.hidesBackButton = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - TextFields delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self setUsersSelectedValues];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

    textField.layer.borderColor=[[UIColor clearColor]CGColor];
//    BOOL valid;    
//    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
//    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:textField.text];
//    valid = [alphaNums isSupersetOfSet:inStringSet];
//    if (!valid) {
//        textField.layer.cornerRadius = 3.0f;
//        textField.layer.masksToBounds = YES;
//        textField.layer.borderColor = [[UIColor redColor]CGColor];
//        textField.layer.borderWidth = 1.0f;
//        self.confirmButton.enabled = NO;
//    } else {
//        textField.layer.borderColor=[[UIColor clearColor]CGColor];
//        self.confirmButton.enabled = YES;
//    }

    [self setUsersSelectedValues];
    [textField resignFirstResponder];
}

- (void)setUsersSelectedValues {
    [self.delegate selectedValue:self.textField.text forKey:self.keyOfField];
}

#pragma mark - Comfirm button

- (IBAction)confirmButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
