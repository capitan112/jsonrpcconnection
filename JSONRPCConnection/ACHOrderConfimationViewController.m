//
//  ACHOrderConfimationViewController.m
//  SimplyBook-me
//
//  Created by Капитан on 10.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHOrderConfimationViewController.h"
#import "ACHDataSource.h"
#import "ACHAppDelegate.h"
#import "Booking.h"
#import "ACHGlobalData.h"

static NSString *const reuseCellIdentifier = @"bookedCell";

@interface ACHOrderConfimationViewController ()

@property (nonatomic, strong) ACHDataSource *dataSource;
@property (nonatomic, strong) NSDictionary *resultBook;

@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *eMail;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *eventName;
@property (weak, nonatomic) IBOutlet UILabel *perfomerName;
@property (weak, nonatomic) IBOutlet UILabel *bookingDataAndTime;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *additionalFileldsLabel;

@end

@implementation ACHOrderConfimationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setNeedsDisplay];
    [self clearBookingDetails];
    [self showBookingDetails];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    if ([self.additionalFields count] == 0) {
        self.additionalFileldsLabel.hidden = YES;
    }
    NSString *confirmed = self.bookingFormServer[@"result"][@"bookings"][0][@"is_confirmed"];
    if ([confirmed isEqualToString:@"0"]) {
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor redColor]};
        self.navigationItem.title = @"Not confirmed";
    } else {
        UIColor *color = [UIColor colorWithRed:37/255.0f green:142/255.0f blue:33/255.0f alpha:1.0f];
        self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:color};
        self.navigationItem.title = @"Confirmed";
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor]};
}

- (void)clearBookingDetails {
    self.userName.text = @"";
    self.eMail.text = @"";
    self.phone.text = @"";
    self.eventName.text = @"";
    self.perfomerName.text = @"";
    self.bookingDataAndTime.text = @"";
}

- (void)showBookingDetails {
    self.userName.text = self.userDetails[@"name"];
    self.eMail.text = self.userDetails[@"email"];
    self.phone.text = self.userDetails[@"phone"];
    self.eventName.text = self.dataOfBooking[@"eventName"];
    self.perfomerName.text = self.dataOfBooking[@"perfomerName"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    NSDate *reservationDate = self.dataOfBooking[@"bookedDateAndTime"];
    NSString *reservationTime = [dateFormatter stringFromDate:reservationDate];

//    NSString *reservationTime = [NSString stringWithFormat:@"%@ %@ %@", self.dataOfBooking[@"requiredDay"], @" at ", self.dataOfBooking[@"requiredTime"]];
    self.bookingDataAndTime.text = reservationTime;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.additionalFields count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *headerCell = [self.tableView dequeueReusableCellWithIdentifier:reuseCellIdentifier];
    if (headerCell == nil) {
        headerCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseCellIdentifier];
    }
    NSString *titleOfFields = self.additionalFields[indexPath.row][@"title"];
    NSString *keyOfFields = self.additionalFields[indexPath.row][@"name"];
    headerCell.textLabel.text = titleOfFields;
    headerCell.detailTextLabel.text = self.userSelectedData[keyOfFields];
    
    return headerCell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - push to Category

- (IBAction)pushedToCategory:(id)sender {
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor]};

    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)sharedPressed:(id)sender {
    NSString *textToShare = kMessageForShare;
    NSURL *website = [NSURL URLWithString:@"https://simplybook.me"];
    NSArray *objectsToShare = @[textToShare, website];
    
    UIActivityViewController *activity = [[UIActivityViewController alloc]
                                          initWithActivityItems:objectsToShare
                                          applicationActivities:nil];
    [self presentViewController:activity animated:YES completion:nil];
    
}

@end
