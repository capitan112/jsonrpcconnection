//
//  Company.h
//  SimplyBook-me
//
//  Created by Капитан on 23.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Booking;

@interface Company : NSManagedObject

@property (nonatomic, retain) NSString * addressOne;
@property (nonatomic, retain) NSString * addressTwo;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * companyName;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, assign) BOOL favorites;
@property (nonatomic, retain) NSString * login;
@property (nonatomic, retain) NSData * logo;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSData * companyDescription;
@property (nonatomic, retain) NSSet *booking;

@end

@interface Company (CoreDataGeneratedAccessors)

- (void)addBookingObject:(Booking *)value;
- (void)removeBookingObject:(Booking *)value;
- (void)addBooking:(NSSet *)values;
- (void)removeBooking:(NSSet *)values;

@end
