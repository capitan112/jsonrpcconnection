//
//  ACHCollectionViewController.h
//  JSONRPCConnection
//
//  Created by Капитан on 04.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACHUserDetailsViewController.h"

@interface ACHCollectionViewSheddule : UICollectionViewController <UserDetail>

@property (nonatomic, strong) NSString *requiredDay;
@property (nonatomic, strong) NSString *perfomerID;
@property (nonatomic, strong) NSString *eventID;
@property (nonatomic, strong) NSDictionary *currentEventsList;
@property (nonatomic, strong) NSString *currentPerfomerName;
@property (nonatomic, strong) NSDictionary *fullPerformersList;
@property (nonatomic, strong) NSDate *bookDate;

- (void)loadRequieredDay:(NSString *)requiredDay;

@end
