//
//  ACHMapViewController.m
//  SimplyBook-me
//
//  Created by Капитан on 26.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHMapViewController.h"
#import "ACHCompanyAnnotation.h"
#import "ACHCompanyInfoViewController.h"

static NSString *const mapInfoSegue = @"mapInfoSegue";
//const float kExponet = 2.3;

@interface ACHMapViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) ACHCompanyInfoViewController *infoDestinationController;

@end

@implementation ACHMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.topItem.title = @"List";
    CLLocationCoordinate2D loc;
    loc.latitude = self.currentLocation.coordinate.latitude;
    loc.longitude = self.currentLocation.coordinate.longitude;
    self.view.backgroundColor = [UIColor whiteColor];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    int seachingDistance = [[defaults objectForKey:@"seachingDistance"] intValue];
    if (seachingDistance <= 1) {
        [defaults setValue:[NSNumber numberWithInt:12]  forKey:@"seachingDistance"];
        [defaults synchronize];
        seachingDistance = 12;
    }
//    NSLog(@"seachingDistance %d", seachingDistance);
    MKCoordinateRegion reg = MKCoordinateRegionMakeWithDistance (CLLocationCoordinate2DMake(loc.latitude, loc.longitude), seachingDistance * 1500, seachingDistance * 1500);
    self.mapView.region = reg;
    self.mapView.delegate = self;
    self.mapView.mapType = MKMapTypeHybrid;

    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.mapView.showsUserLocation = YES;
    
    int arraySize = (int)[self.companyList count];
    for (int i = 0; i < arraySize; i++) {
        CLLocationCoordinate2D location;
        location.latitude = [self.companyList[i][@"lat"] doubleValue];
        location.longitude = [self.companyList[i][@"lng"] doubleValue];
        ACHCompanyAnnotation *annotation = [[ACHCompanyAnnotation alloc]
                                            initWithCoordinates:location
                                                          title:_companyList[i][@"name"]
                                                       subTitle:_companyList[i][@"description"]
                                                          login:_companyList[i][@"login"]];
        annotation.pinColor = MKPinAnnotationColorGreen;
        [self.mapView addAnnotation:annotation];
    }
    
}

//- (void)mapView:(MKMapView *)mapView didUpdateUserLocation: (MKUserLocation *)userLocation {
//    self.mapView.centerCoordinate = userLocation.location.coordinate;
//}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view
calloutAccessoryControlTapped:(UIControl *)control {
    ACHCompanyAnnotation *annotation = view.annotation;
//    NSLog(@"annotation login %@", annotation.login);
    [self performSegueWithIdentifier:mapInfoSegue sender:self];
    self.infoDestinationController.companyLogin = annotation.login;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    MKAnnotationView *result = nil;
    if ([annotation isKindOfClass:[ACHCompanyAnnotation class]] == NO) {
        return result;
    }
    if ([mapView isEqual:self.mapView] == NO) {
        return result;
    }
    ACHCompanyAnnotation *senderAnnotation = (ACHCompanyAnnotation *)annotation;
    NSString *pinReusableIdentifier = [ACHCompanyAnnotation
                                       reusableIdentifierforPinColor:senderAnnotation.pinColor];
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:pinReusableIdentifier];
    if (annotationView == nil){
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:senderAnnotation
                                                         reuseIdentifier:pinReusableIdentifier];
        [annotationView setCanShowCallout:YES];
    }
    annotationView.pinColor = senderAnnotation.pinColor;
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    result = annotationView;
    
    return result;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:mapInfoSegue  ]) {
        self.infoDestinationController = [segue destinationViewController];
    }
}

@end
