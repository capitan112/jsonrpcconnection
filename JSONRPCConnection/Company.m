//
//  Company.m
//  SimplyBook-me
//
//  Created by Капитан on 23.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "Company.h"
#import "Booking.h"


@implementation Company

@dynamic addressOne;
@dynamic addressTwo;
@dynamic city;
@dynamic companyName;
@dynamic email;
@dynamic favorites;
@dynamic login;
@dynamic logo;
@dynamic phone;
@dynamic companyDescription;
@dynamic booking;

@end
