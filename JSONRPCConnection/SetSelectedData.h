//
//  SetSelectedData.h
//  SimplyBook-me
//
//  Created by Капитан on 09.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SetSelectedData <NSObject>
@required

- (void)selectedValue:(NSString *)value forKey:(NSString *)key;

@end
