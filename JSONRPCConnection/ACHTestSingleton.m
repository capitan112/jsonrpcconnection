//
//  ACHTestSingleton.m
//  SimplyBook-me
//
//  Created by Капитан on 21.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "ACHDataSource.h"
#import "ACHPreferencesViewController.h"

@interface ACHTestSingleton : XCTestCase

@end

@implementation ACHTestSingleton

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    ACHPreferencesViewController *prefernces = [[ACHPreferencesViewController alloc] init];
    
//tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
//    ACHDataSource *secondSingleton = [[ACHDataSource alloc] init];
//    XCTAssertThrows([[ACHDataSource alloc] init], @"Invoked singleton method");

    XCTAssert(prefernces, @"Pass");
}

//- (void)testPerformanceExample {
//    // This is an example of a performance test case.
//    [self measureBlock:^{
//        // Put the code you want to measure the time of here.
//    }];
//}

@end
