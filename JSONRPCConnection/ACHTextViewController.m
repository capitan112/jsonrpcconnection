//
//  ACHTextViewController.m
//  SimplyBook-me
//
//  Created by Капитан on 09.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHTextViewController.h"

@interface ACHTextViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@end

@implementation ACHTextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textView.text = self.selectedValue;
    [self confirmButtonConfiguration];
}

- (void)confirmButtonConfiguration {
    self.textView.text = self.selectedValue;
    self.confirmButton.layer.cornerRadius = 5;
    self.confirmButton.layer.borderWidth = 1;
    self.confirmButton.layer.borderColor = self.confirmButton.tintColor.CGColor;
    self.navigationItem.hidesBackButton = YES;
}


#pragma mark - textView delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)confirmButtonPressed:(id)sender {
    [self.delegate selectedValue:self.textView.text forKey:self.keyOfField];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
