//
//  ACHHistoryDetailViewController.h
//  SimplyBook-me
//
//  Created by Капитан on 17.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Booking.h"

@protocol ACHHistoryDetailViewControllerDelegate <UIAlertViewDelegate>

- (void)ACHHistoryDetailViewControllerDelete:(Booking *)historyToDelete;

@end


@interface ACHHistoryDetailViewController : UIViewController

@property (nonatomic, strong) Booking *selectedHistory;
@property (nonatomic, weak) id <ACHHistoryDetailViewControllerDelegate> delegate;

@end
