//
//  ACHFavoritesTableViewController.h
//  SimplyBook-me
//
//  Created by Капитан on 21.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface ACHFavoritesTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

- (void)refreshData;

@end
