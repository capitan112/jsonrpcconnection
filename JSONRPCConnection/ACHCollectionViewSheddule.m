//  ACHCollectionViewController.m
//  JSONRPCConnection
//
//  Created by Капитан on 04.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHCollectionViewSheddule.h"
#import "ACHDataSource.h"
#import "ACHCollectionViewCellSheddule.h"
#import "ACHCollectionHeaderSheddule.h"
#import "ACHViewControllerReservation.h"

static NSString *const headerIdentifier = @"HeaderIdSheddule";
static NSString *const reuseCellIdentifier = @"CellIdSheddule";
static NSString *const userDetailsKey = @"userDetailKey";
static NSString *const reservationSegue = @"reservationSegue";
static NSString *const userDetailFromTimeSegue = @"userDetailFromTimeSegue";

@interface ACHCollectionViewSheddule ()

@property (nonatomic, strong) ACHDataSource *dataSource;
@property (nonatomic, strong) NSDictionary *sourceShedduleData;
@property (nonatomic, strong) NSArray *workShedduleData;
@property (nonatomic, strong) ACHViewControllerReservation *reservationController;
@property (nonatomic, strong) NSMutableDictionary *sendDataReservation;
@property (nonatomic, strong) UIView *subview;
@property (nonatomic, strong) NSDictionary *userDetails;
@property (nonatomic, strong) NSUserDefaults *defaults;
@property (nonatomic) BOOL isEmptyUserDetails;
@property (nonatomic, strong) ACHUserDetailsViewController *userDetailController;

@end

@implementation ACHCollectionViewSheddule


- (void)viewDidLoad {
    [super viewDidLoad];
    _dataSource = [ACHDataSource sharedInstance];
    _sourceShedduleData = [[NSDictionary alloc] init];
    _sendDataReservation = [[NSMutableDictionary alloc] init];
    self.subview = [[UIView alloc] initWithFrame:self.collectionView.frame];
    [self.collectionView addSubview:self.subview];
    [self.collectionView bringSubviewToFront:self.subview];
    self.subview.backgroundColor = [UIColor clearColor];
    self.subview.hidden = YES;
    UILabel *noTimeLabel = [[UILabel alloc] initWithFrame: CGRectMake(80, 100, 220, 20)];
    noTimeLabel.text = @"No time for booking";
    [self.subview addSubview:noTimeLabel];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadCollectionTable)
                                                 name:@"newDataFromServer"
                                               object:nil];
    [self loadRequieredDay:_requiredDay];
    [self userDetailsLoad];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_dataSource hideProgressHUD];
}

- (void)userDetailsLoad {
    self.defaults = [NSUserDefaults standardUserDefaults];
    NSData *userDetailDictionaryData = [self.defaults objectForKey:userDetailsKey];
    self.isEmptyUserDetails = NO;
    if (userDetailDictionaryData) {
        self.userDetails = [[NSKeyedUnarchiver unarchiveObjectWithData:userDetailDictionaryData] mutableCopy];
        for (NSString *item in [self.userDetails allKeys]) {
            if ([_userDetails[item] length] == 0) {
                self.isEmptyUserDetails = YES;
            }
        }
    } else {
        self.userDetails = [[NSMutableDictionary alloc] init];
        self.isEmptyUserDetails = YES;
    }
}

- (void)userDetailsDictionary:(NSDictionary *)dict {
    self.userDetails = [dict mutableCopy];
}

- (void)sortRowSourceArray {
    _workShedduleData = nil;

    NSComparator finderSortBlock = ^(id a, id b) {
        return [a compare:b options: NSNumericSearch];
    };
    NSArray *sortedKeys = [[_sourceShedduleData allKeys] sortedArrayUsingComparator:finderSortBlock];
    
    _workShedduleData = [sortedKeys copy];
}

- (void)loadRequieredDay:(NSString *)requiredDay {
    _sourceShedduleData = nil;
    _workShedduleData = nil;
    [self.collectionView reloadData];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    [_dataSource showProgressHUD];
    _sourceShedduleData = [_dataSource startTimeMatrixOfDay:requiredDay
                                                      event:_eventID
                                                   perfomer:_perfomerID];
        
//    NSLog(@"_sourceShedduleData: %@", _sourceShedduleData);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"newDataFromServer" object:self];
    });
}

#pragma mark - delegate method

- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([_sourceShedduleData count] == 0) {
        self.subview.hidden = NO;
    } else {
        self.subview.hidden = YES;
    }
    return [_sourceShedduleData count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ACHCollectionViewCellSheddule *cell = (ACHCollectionViewCellSheddule *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseCellIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [UIColor colorWithRed:204.0f/255.0f green:225.0f/255.0f blue:238.0f/255.0f alpha:1.0];
    cell.cellLabel.text = _workShedduleData[indexPath.row];
    [cell.layer setCornerRadius:5];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
    if (isNetwork) {
        
        
        NSString *time = _workShedduleData[indexPath.row];

        NSArray *timeArray  = [time componentsSeparatedByString: @":"];
        NSString *hours = [timeArray objectAtIndex: 0];
        NSString *minutes = [timeArray objectAtIndex:1];
        
        unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *comps = [calendar components:unitFlags fromDate:self.bookDate];
        comps.hour   = [hours intValue];
        comps.minute = [minutes intValue];
        comps.second = 00;
        NSDate *newDate = [calendar dateFromComponents:comps];
        [_sendDataReservation setObject:newDate forKey:@"bookedDateAndTime"];
//        NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
//        NSDate *dateInLocalTimezone = [newDate dateByAddingTimeInterval:timeZoneSeconds];
//        NSLog(@"newDate %@", dateInLocalTimezone);
//        [_sendDataReservation setObject:dateInLocalTimezone forKey:@"bookedDateAndTime"];
        
        [_sendDataReservation setObject:_workShedduleData[indexPath.row] forKey:@"requiredTime"];
        [_sendDataReservation setObject:_requiredDay forKey:@"requiredDay"];
        [_sendDataReservation setObject:_eventID forKey:@"eventID"];
        [_sendDataReservation setObject:_currentEventsList[@"name"] forKey:@"eventName"];
        [_sendDataReservation setObject:_perfomerID forKey:@"perfomerID"];
        [_sendDataReservation setObject:_currentPerfomerName forKey:@"perfomerName"];

        

        if (self.isEmptyUserDetails) {
            [self performSegueWithIdentifier:userDetailFromTimeSegue sender:nil];
            self.userDetailController.dataReservation = [_sendDataReservation mutableCopy];
            self.userDetailController.fullPerformersList = [self.fullPerformersList copy];
        } else {
            [self performSegueWithIdentifier:reservationSegue sender:nil];
            _reservationController.dataReservation = [_sendDataReservation mutableCopy];
            _reservationController.fullPerformersList = [self.fullPerformersList copy];

        }
    }
}

#pragma mark header delegate

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    ACHCollectionHeaderSheddule *headerView = (ACHCollectionHeaderSheddule *)[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:headerIdentifier forIndexPath:indexPath];
    headerView.serviceLabel.text = _currentEventsList[@"name"];

    if ([_currentPerfomerName isEqualToString:@""] || [_currentPerfomerName isEqual:[NSNull null]]) {
//        _currentPerfomerName = @"Any perfomer";
        headerView.staffSignLabel.hidden = YES;
    }
    
    headerView.performerLabel.text = _currentPerfomerName;
    
    return headerView;
}

- (void)reloadCollectionTable {
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self sortRowSourceArray];
        [self.collectionView reloadData];
        [_dataSource hideProgressHUD];
   });
}

#pragma mark - segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:reservationSegue]) {
        self.reservationController = segue.destinationViewController;
    }
    
    if([segue.identifier isEqualToString:userDetailFromTimeSegue]) {
        self.userDetailController = segue.destinationViewController;
        self.userDetailController.delegate = self;
    }
}

@end
