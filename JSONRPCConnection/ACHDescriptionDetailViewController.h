//
//  ACHDescriptionDetailViewController.h
//  SimplyBook-me
//
//  Created by Капитан on 03.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACHDescriptionDetailViewController : UIViewController

@property (nonatomic, strong) NSDictionary *allCategory;
@property (nonatomic, strong) NSArray *sendedListOfEvents;
//@property (nonatomic, strong) NSMutableDictionary *companyInfo;

@end
