//
//  CAJSONRPCConnection.m
//  JSONRPCConnection
//
//  Created by Капитан on 04.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//


#import "ACHJSONRPCConnection.h"
#import "ProgressHUD.h"

static const NSString *urlToken        = @"http://user-api.ru.simplybook.me/login";
static const NSString *urlRegistration = @"https://user-api.simplybook.me/";
static const NSString *urlCatalog      = @"http://user-api.simplybook.me/catalog";
static const NSString *urlUser         = @"https://user-api.simplybook.me/user";
static const NSString *secureKeyAPI    = @"alexey_dev_1mhf41ec6ab9d6e1cde99baf6cb128206a5";
static const NSString *tokenKey        = @"tokenKey";

static const int serverTimeout = 20;
static const BOOL debugTools = NO;

@interface ACHJSONRPCConnection()

@property (nonatomic, copy) NSString *companyLogin;
@property (nonatomic, strong) NSMutableData *receivedData;

@end

@implementation ACHJSONRPCConnection

//+ (id)sharedInstanceWithLogin:(NSString *)companyLogin {
//    static ACHJSONRPCConnection *sharedInstance = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        sharedInstance = [[self alloc] initWithCompanyLogin:companyLogin];
//    });
//
//    return sharedInstance;
//}

- (instancetype)initWithCompanyLogin:(NSString *)companyLogin {
    self = [super init];
    if (self) {
        self.receivedData = [[NSMutableData alloc] init];
        self.companyLogin = [companyLogin copy];
//        NSLog(@"self.companyLogin %@", self.companyLogin);
    }
    
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [NSException raise:@"Invoked singleton method" format:@"Invoked singleton method"];
    }
    
    return self;
}

#pragma mark - global token request

- (NSString *)getApplicationTokenWithKeyAPI {
    NSInteger idRequest = arc4random();
    NSDictionary *rpcDict = @{
                              @"jsonrpc" : @"2.0",
                              @"id" : @(idRequest),
                              @"method" : @"getApplicationToken",
                              @"params" : @[secureKeyAPI]
                              };
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rpcDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSDictionary *defaultHeader = @{
                                    @"Accept" : @"application/json",
                                    @"Content-Type": @"application/json"
                                    };
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                    [NSURL URLWithString:[urlToken copy]]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:serverTimeout];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:defaultHeader];
    [request setHTTPBody:jsonData];

    NSURLResponse *response = nil;
    NSError *serverDataError;
    NSData *serverData = [NSURLConnection sendSynchronousRequest:request
                                               returningResponse:&response
                                                           error:&serverDataError];
    if (!serverDataError) {
        NSError *dictionaryParceError;
        NSDictionary *serverDict = [NSJSONSerialization JSONObjectWithData:serverData
                                                                   options:NSJSONReadingMutableContainers
                                                                     error:&dictionaryParceError];
        if (serverDict[@"error"]) {
            UIAlertView *alert = [self alertWithTitle:@"Server error" message:serverDict[@"error"]];
            [alert show];
            return nil;
        }
        
        return serverDict[@"result"];
        
    } else {
        NSLog (@"serverDataError %@", serverDataError);
        UIAlertView *alert = [self alertWithTitle:@"No internet connection" message:@"Please check connection"];
        [alert show];
        return nil;
    }
}

#pragma mark - data request

- (NSDictionary *)requestWithDict:(NSDictionary *)dict fromURL:(enum sourceURL)URLEnum {
    NSString *sourceURL;
    if (URLEnum == registration) {
        sourceURL = [urlRegistration copy];
    } else if (URLEnum == catalog){
        sourceURL = [urlCatalog copy];
    } else if (URLEnum == user){
        sourceURL = [urlUser copy];
    }
    
//    NSLog(@"sourceURL %@", sourceURL);
    NSMutableDictionary *rpcDict = nil;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [defaults objectForKey:@"tokenKey"];
    if (!token) {
        token = [self getApplicationTokenWithKeyAPI];
        [defaults setObject:token forKey:@"tokenKey"];
    }
    
    NSInteger idRequest = arc4random();
    NSDictionary *dictWithParamets = @{
                                        @"jsonrpc" : @"2.0",
                                        @"id" : @(idRequest),
                                       };
    rpcDict = [NSMutableDictionary dictionaryWithDictionary:dictWithParamets];
    [rpcDict addEntriesFromDictionary:dict];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:rpcDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *login;
    if (!_companyLogin) {
        login = @"";
        
    } else {
        login =_companyLogin;
    }
    NSDictionary *defaultHeader = @{
                                    @"Accept" : @"application/json",
                                    @"Content-Type": @"application/json",
                                    @"X-Company-Login" : login,
                                    @"X-Application-Token" : token
                                    };
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                    initWithURL:[NSURL URLWithString:sourceURL]
                                    cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                timeoutInterval:serverTimeout];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:defaultHeader];
    [request setHTTPBody:jsonData];

    NSDictionary *resultDict = [[self dictFromServer:request] copy];
    
    NSString *accessDineid = resultDict[@"error"][@"message"];
    
    if (!resultDict || [resultDict isEqual:[NSNull null]]) {
        NSLog(@"defaultHeader %@", defaultHeader);
        NSLog(@"rpcDict %@", rpcDict);
        NSLog(@"resultDict %@", resultDict);
    }

    
    if (debugTools) {
        NSLog(@"defaultHeader %@", defaultHeader);
        NSLog(@"rpcDict %@", rpcDict);
        NSLog(@"resultDict %@", resultDict);
    }
    
    if ([accessDineid isEqualToString:@"Access denied"]) {
        token = [self getApplicationTokenWithKeyAPI];
        [defaults setObject:token forKey:@"tokenKey"];
        defaultHeader = @{
                              @"Accept" : @"application/json",
                              @"Content-Type" : @"application/json",
                              @"X-Company-Login" : login,
                              @"X-Application-Token" : token
                          };
        [request setAllHTTPHeaderFields:defaultHeader];
        return [[self dictFromServer:request] copy];
    }
//    NSLog(@"resultDict %@", resultDict[@"result"]);
    if (resultDict[@"error"]) {
        NSString *resultMessage = [NSString stringWithFormat:@"error with login %@: \n%@", self.companyLogin, resultDict[@"error"][@"message"]];
        UIAlertView *alert = [self alertWithTitle:@"Server error" message:resultMessage];
        dispatch_async(dispatch_get_main_queue(), ^{
            [alert show];
        });
        return nil;
    }
    
    
    return [resultDict copy];
}

- (NSDictionary *)dictFromServer:(NSURLRequest *)request {
    NSURLResponse *response = nil;
    NSError *serverDataError;
    
    NSData *serverData = [NSURLConnection sendSynchronousRequest:request
                                               returningResponse:&response
                                                           error:&serverDataError];
    if (debugTools) {
//        NSLog(@"response%@ serverDataError %@", response, serverDataError);
        const unsigned char *ptr = [serverData bytes];
        for(int i=0; i < [serverData length]; ++i) {
            unsigned char c = *ptr++;
            //        NSLog(@"char=%c hex=%x", c, c);
            printf("%c", c);
        }
    }
    if (!serverData) {
        UIAlertView *alert = [self alertWithTitle:@"No server data" message:@"Please try again"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [alert show];
        });
        return  nil;
    }
    
    NSError *dictionaryParceError;
    NSDictionary *serverDictionary = [NSJSONSerialization JSONObjectWithData:serverData
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&dictionaryParceError];
//debug tools
//    NSLog(@"serverDictionary %@ server Error%@", serverDictionary, dictionaryParceError);
    return [serverDictionary copy];
}


- (UIAlertView *)alertWithTitle:(NSString*)title message:(NSString*)message {
    UIAlertView *alert =[[UIAlertView alloc ] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles: nil];
    return alert;
}

@end
