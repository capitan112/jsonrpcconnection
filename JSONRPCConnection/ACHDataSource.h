//
//  CADataSource.h
//  JSONRPCConnection
//
//  Created by Капитан on 04.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ACHDataSource : NSObject

@property (nonatomic, strong) NSMutableDictionary *shedduleTimeDict;
@property (nonatomic, strong) NSArray *sortedKeyHoursArray;
@property (nonatomic, strong) NSMutableArray *resultArray;
@property (nonatomic, strong) NSString *companyLogin;
@property (nonatomic, strong) NSString *keyAPI;

+ (id)sharedInstance;
+ (id)sharedInstanceWithLogin:(NSString *)companyLogin;
+ (void)selfDestruction;
+ (BOOL)isNetworkAvaiable;
//- (id)init;
- (NSDictionary *)startTimeMatrixOfDay:(NSString *)
                     requiredDay event:(NSString *)eventId
                              perfomer:(NSString *)unitId;
- (NSArray *)getCategoryList;
- (NSDictionary *)getEventList;
- (NSDictionary *)getPerformersList;
- (NSDictionary *)getAnyUnitData;

- (NSArray *)availableUnitsForEvent:(NSString *)eventId
                               date:(NSString *)date
                               time:(NSString *)time
                              count:(NSString *)count;


- (BOOL)isPluginActivated:(NSString *)pluginName;
- (BOOL)isPaymentRequired:(NSString *)eventId;

- (NSDictionary *)bookWithEvent:(NSString *)eventId
                         unitId:(NSString *)unitId
                    requiredDay:(NSString *)requiredDay
                   requiredTime:(NSString *)requiredTime
                    userDetails:(NSDictionary *)userDetails
               additionalFields:(NSDictionary *)additionalFields;

- (NSDictionary *)timeMatrixOfDay:(NSDictionary *)dict;
- (UIImage *)pictureFormURL:(NSString *)stringURL;
- (NSArray *)additionalFields:(NSString *)eventId;
- (NSDictionary *)getCompanyInfo;
- (NSDictionary *)cancelBookingID:(NSString *)bookingId sign:(NSString *)sign;
- (NSArray *)companyiesWithContainsWord:(NSString *)containsWord
                              seachingRadius:(int)radius
                             currentLatitude:(double)latitude
                            currentLongitude:(double)longitude
                                    startRow:(int)startRow
                                 requestRows:(int)requestRows;
- (NSDictionary *)bookingInfoWithID:(NSString *)bookingID bookingHash:(NSString *)bookingHash;
- (void)setAllowNotificationWithToken:(NSString *)deviceToken
                         deviceType:(NSString *)deviceType
                       companyLogin:(NSString *)companyLogin
                 allowNotifications:(NSString *)isAllow;

- (void)showProgressHUD;
- (void)hideProgressHUD;

@end

