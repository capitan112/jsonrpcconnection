//
//  ACHTextViewController.h
//  SimplyBook-me
//
//  Created by Капитан on 09.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetSelectedData.h"

@interface ACHTextViewController : UIViewController

@property (nonatomic, strong) NSString *keyOfField;
@property (nonatomic, strong) NSString *selectedValue;
@property (nonatomic, retain) id <SetSelectedData> delegate;

@end
