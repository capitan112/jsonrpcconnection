//
//  ACHTableViewControllerPerfomers.m
//  SimplyBookApp
//
//  Created by Капитан on 23.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHTableViewControllerPerfomers.h"
#import "ACHDataSource.h"
#import "ACHViewControllerCalendar.h"
#import "ProgressHUD.h"
#import "ACHdescriptionPerfomerVC.h"
#import "ACHGlobalData.h"

static NSString *const reuseCellIdentifier = @"cellPerfomers";
static NSString *const descriptionPerfomerSegue = @"descriptionPerfomerSegue";
static NSString *const shedduleSegue = @"shedduleSegue";
static NSString *const anyPerfomer = @"Any perfomer";

static const int rowHeight = 100;

@interface ACHTableViewControllerPerfomers ()

@property (nonatomic, strong) ACHDataSource *dataSource;
//@property (nonatomic, strong) NSDictionary *fullPerformersList;
@property (nonatomic, strong) NSMutableArray *currentPerfomers;
@property (nonatomic, strong) NSString *currentPerfomer;
@property (nonatomic, strong) ACHViewControllerCalendar *destinationController;
@property (nonatomic, strong) ACHdescriptionPerfomerVC *descriptionVC;
@property (nonatomic, strong) UIView *noPerfomerView;
@property (nonatomic) BOOL isAnyUnit;

@end

@implementation ACHTableViewControllerPerfomers

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Select Staff";
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _dataSource = [ACHDataSource sharedInstance];
    [self loadServices];
}

- (void)loadServices {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [_dataSource showProgressHUD];
//        _fullPerformersList = [_dataSource getPerformersList];
        _currentPerfomers = [_sendedPerfomers mutableCopy];
//        NSLog(@"_fullPerformersList %@", _fullPerformersList);
        if ([_sendedPerfomers count] == 0) {
            _currentPerfomers = [[_fullPerformersList allKeys] mutableCopy];
            
        }
//        NSLog (@"_currentPerfomers %@", _currentPerfomers);
        self.isAnyUnit = [_dataSource isPluginActivated:@"any_unit"];
        if (self.isAnyUnit) {
            [_currentPerfomers insertObject:anyPerfomer atIndex:0];
        }
//        NSLog(@"_currentPerfomers %@", _currentPerfomers);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [_dataSource hideProgressHUD];
            
        });
    });
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_currentPerfomers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseCellIdentifier];
    }
    if (self.isAnyUnit && indexPath.row == 0) {
        cell.textLabel.text = _currentPerfomers[indexPath.row];
        cell.detailTextLabel.text = _currentPerfomers[indexPath.row];
        cell.imageView.image = [UIImage imageNamed:@"noPhotoCategory.png"];
    } else {
        NSString *keyPerfomers = _currentPerfomers[indexPath.row];
        cell.textLabel.text = _fullPerformersList[keyPerfomers][@"name"];
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        cell.textLabel.minimumScaleFactor = 0.5;
        
        NSString *perfomerDescription = _fullPerformersList[keyPerfomers][@"description"];
        if ([perfomerDescription isEqual:[NSNull null]]) {
            perfomerDescription = @"";
        }
        cell.detailTextLabel.text = perfomerDescription;
        cell.accessoryType = UITableViewCellAccessoryDetailButton;
        cell.detailTextLabel.numberOfLines = 4;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.imageView.image = [UIImage imageNamed:@"noPhotoCategory.png"];
        
        NSString *pictureName = _fullPerformersList[keyPerfomers][@"picture"];
        if (![pictureName isEqual:[NSNull null]]) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                UIImage *image = [_dataSource pictureFormURL:_fullPerformersList[keyPerfomers][@"picure_path"]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    cell.imageView.image = image;
                });
            });
        }
    }
    
    if (isCellBorder) {
        cell.imageView.layer.borderWidth = 0.5f;
        cell.imageView.layer.MasksToBounds = YES;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return rowHeight;
}

- (void)tableView:(UITableView *)tableView
accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
    if (isNetwork) {
        [self performSegueWithIdentifier:descriptionPerfomerSegue sender:self];
        NSString *keyPerfomers = _currentPerfomers[indexPath.row];
//        NSString *keyPerfomers = _currentPerfomers[indexPath.row];
        self.descriptionVC.fullPerformersList = _fullPerformersList[keyPerfomers];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
    if (isNetwork) {
        [self performSegueWithIdentifier:shedduleSegue sender:self];
        NSString *perfomerID = _currentPerfomers[indexPath.row];
//        NSLog(@"perfomerID at didSelectRowAtIndexPath %@", perfomerID);
        if ([perfomerID isEqualToString:anyPerfomer]) {
            perfomerID = @"";
        }
        _destinationController.perfomerID = perfomerID;
        _destinationController.eventID = _eventID;
        _destinationController.currentEventsList = _currentEventsList;
        _destinationController.currentPerfomerName = _currentPerfomer;
        _destinationController.fullPerformersList = _fullPerformersList;
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//    NSLog(@"indexPath row %ld", (long)indexPath.row);
    _currentPerfomer = _fullPerformersList[_currentPerfomers[indexPath.row]][@"name"];
    NSString *perfomerID = _currentPerfomers[indexPath.row];
//    NSLog(@"indexPath.row %ld", (long)indexPath.row);

    if (!_currentPerfomer){
        _currentPerfomer = @"";
        perfomerID = @"";
    }
    
    if ([segue.identifier isEqualToString:shedduleSegue]) {
        _destinationController = [segue destinationViewController];
    }
    
    if ([segue.identifier isEqualToString:descriptionPerfomerSegue]) {
        self.descriptionVC = [segue destinationViewController];
        self.descriptionVC.perfomerID = perfomerID;
        self.descriptionVC.eventID = _eventID;
        self.descriptionVC.currentEventsList = _currentEventsList;
        self.descriptionVC.currentPerfomerName = _currentPerfomer;
//        self.descriptionVC.companyInfo = [self.companyInfo copy];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
