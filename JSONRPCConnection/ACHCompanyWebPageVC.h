//
//  ACHCompanyWebPageVC.h
//  SimplyBookMobile 
//
//  Created by Капитан on 18.12.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACHCompanyWebPageVC : UIViewController

@property (nonatomic, strong) NSString *fullURL;

@end
