//
//  ACHSeachListTableViewController.h
//  SimplyBook-me
//
//  Created by Капитан on 26.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ACHCompanyInfoViewController.h"
#import "ACHMapViewController.h"

@interface ACHSeachListTableViewController : UITableViewController <CLLocationManagerDelegate>

@property (nonatomic, strong) ACHCompanyInfoViewController *infoController;
@property (nonatomic, strong) ACHMapViewController *mapController;

@end
