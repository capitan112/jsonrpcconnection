//
//  ACHMapViewController.h
//  SimplyBook-me
//
//  Created by Капитан on 26.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ACHMapViewController : UIViewController <MKMapViewDelegate>

@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) NSArray *companyList;

@end
