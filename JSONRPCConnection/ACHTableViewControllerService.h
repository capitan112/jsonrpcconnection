//
//  ACHTableViewControllerService.h
//  SimplyBookApp
//
//  Created by Капитан on 23.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACHTableViewControllerService : UITableViewController

@property (nonatomic, strong) NSArray *sendedListOfEvents;
//@property (nonatomic, strong) NSMutableDictionary *companyInfo;

@end
