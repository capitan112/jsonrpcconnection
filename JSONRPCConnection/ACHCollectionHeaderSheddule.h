#import <UIKit/UIKit.h>

@interface ACHCollectionHeaderSheddule : UICollectionReusableView

@property (nonatomic, copy) NSString *text;

@property (weak, nonatomic) IBOutlet UILabel *serviceLabel;
@property (weak, nonatomic) IBOutlet UILabel *performerLabel;
@property (weak, nonatomic) IBOutlet UILabel *staffSignLabel;

@end
