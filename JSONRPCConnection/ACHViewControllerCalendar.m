//
//  ViewController.m
//  LSWeekView-Example
//
//  Created by Christoph Zelazowski on 6/16/14.
//  Copyright (c) 2014 Lumen Spark. All rights reserved.
//

#import "ACHViewControllerCalendar.h"
#import "LSWeekView.h"
#import "ACHCollectionViewSheddule.h"
#import "ProgressHUD.h"

@interface ACHViewControllerCalendar ()

@property (nonatomic, weak) IBOutlet LSWeekView *weekView;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateFormatter *dayFormatter;
@property (nonatomic, strong) ACHCollectionViewSheddule *containerViewController;
@property (nonatomic, strong) NSString *selectedDay;

@end

@implementation ACHViewControllerCalendar

- (void)viewDidLoad {
    [super viewDidLoad];
    __weak typeof(self) weakSelf = self;
    self.weekView.didChangeSelectedDateBlock = ^(NSDate *selectedDate) {
        [weakSelf updateDate];
    };
    [self updateDate];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];
    [ProgressHUD dismiss];
}

- (void)updateDate {
    self.selectedDay = [self.dayFormatter stringFromDate:self.weekView.selectedDate];
    [self.containerViewController loadRequieredDay:_selectedDay];
    self.containerViewController.requiredDay = _selectedDay;
    self.containerViewController.bookDate = self.weekView.selectedDate;
}

- (IBAction)todayButtonAction:(id)sender {
    NSDate *date = [NSDate date];
    if ([self.weekView setSelectedDate:date animated:YES] == NO) {
      [self.weekView animateSelectedDateMarker];
    }
    [self updateDate];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"perfomerShedduleSegue"]) {
        _containerViewController = [segue destinationViewController];
        self.view.tintColor = [UIColor redColor];
        self.dateFormatter = [[NSDateFormatter alloc] init];
        self.dayFormatter = [[NSDateFormatter alloc] init];
        NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:@"EEEE MMMM d yyyy" options:0 locale:[NSLocale currentLocale]];
        
        NSString *dayFormat = [NSDateFormatter dateFormatFromTemplate:@"MM dd yyyy" options:0 locale:nil];
        [self.dateFormatter setDateFormat:dateFormat];
        [self.dayFormatter setDateFormat:dayFormat];
        
//        NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
//        NSDate *dateInLocalTimezone = [self.weekView.selectedDate dateByAddingTimeInterval:timeZoneSeconds];
//        NSLog(@"_selectedDay %@",self.weekView.selectedDate);
//        NSLog(@"dateInLocalTimezone %@",dateInLocalTimezone);
        
        self.selectedDay = [self.dayFormatter stringFromDate:self.weekView.selectedDate];
        self.containerViewController.bookDate = self.weekView.selectedDate;        
        self.containerViewController.requiredDay = _selectedDay;
        self.containerViewController.perfomerID = _perfomerID;
        self.containerViewController.eventID = _eventID;
        self.containerViewController.currentEventsList = _currentEventsList;
        self.containerViewController.currentPerfomerName = _currentPerfomerName;
        self.containerViewController.fullPerformersList = self.fullPerformersList;
    }
}

@end
