//
//  ACHCompanyInfoViewController.m
//  SimplyBook-me
//
//  Created by Капитан on 13.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHCompanyInfoViewController.h"
#import "ACHDataSource.h"
#import "ACHAppDelegate.h"
#import "Booking.h"
#import "Company.h"
#import "ACHTableViewControllerCategory.h"
#import "ACHTableViewControllerService.h"
#import "ACHGlobalData.h"

static NSString *const categorySegue = @"categorySegue";
static NSString *const infoServiceSegue = @"infoServiceSegue";
static NSString *const serviceCategory = @"event_category";

@interface ACHCompanyInfoViewController () {
    BOOL isActivePluginCategory;
}

@property (nonatomic, strong) ACHDataSource *dataSource;
@property (nonatomic, strong) NSMutableDictionary *companyInfo;
@property (nonatomic, strong) ACHAppDelegate *application;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) Company *company;
@property (nonatomic, strong) ACHTableViewControllerService *serviceController;
@property (nonatomic, strong) ACHTableViewControllerCategory *categoryController;
@property (nonatomic, strong) NSArray *allCategory;

@property (weak, nonatomic) IBOutlet UILabel *addressOne;
@property (weak, nonatomic) IBOutlet UILabel *city;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UILabel *favoritesLabel;
@property (weak, nonatomic) IBOutlet UIButton *addFavoritesButton;
@property (weak, nonatomic) IBOutlet UIButton *bookButton;
@property (weak, nonatomic) IBOutlet UITextView *phoneNumber;
@property (weak, nonatomic) IBOutlet UITextView *email;

@end

@implementation ACHCompanyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self clearLabels];
    [self loadManagedObjectContext];
// debug
    if (!self.companyLogin) {
        NSLog(@"self.companyLogin %@", self.companyLogin);
    }
    
    static NSString *sameCompanyLogin;
    if (![sameCompanyLogin isEqualToString:self.companyLogin]){
        [ACHDataSource selfDestruction];
        sameCompanyLogin = self.companyLogin;
    }
    [self bookButtomCreate];
    
    self.dataSource = [ACHDataSource sharedInstanceWithLogin:self.companyLogin];
//    self.dataSource = [ACHDataSource sharedInstanceWithLogin:@"chuchapoll"];
    [self.dataSource hideProgressHUD];
    self.companyInfo = [[NSMutableDictionary alloc] init];
    [self netChecking];
    isActivePluginCategory = [_dataSource isPluginActivated:serviceCategory];
}

- (void)bookButtomCreate {
    self.bookButton.layer.cornerRadius = 5;
    self.bookButton.layer.borderWidth = 1;
    self.bookButton.layer.borderColor = self.bookButton.tintColor.CGColor;
    self.bookButton.hidden = YES;
}

- (void)netChecking {
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
    if ([self isExistLoginAtDatabase:self.companyLogin] && !isNetwork) {
        [self loadCompanyInfoFromCoreData];
    } else if ([self isExistLoginAtDatabase:self.companyLogin] && isNetwork) {
        [self loadCompanyInfoFromCoreData];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self compareAndUpgradeCompanyInfoAtDataBase:self.companyLogin];
            dispatch_async(dispatch_get_main_queue(), ^ {
                [self loadCompanyInfoFromCoreData];
            });
        });
    } else if (![self isExistLoginAtDatabase:self.companyLogin] && isNetwork) {
        [self loadCompanyInfoFromServer];
    }
}

- (void)loadManagedObjectContext {
    self.application = (ACHAppDelegate *) [[UIApplication sharedApplication]delegate];
    self.managedObjectContext = self.application.managedObjectContext;
}

- (BOOL)isExistLoginAtDatabase:(NSString *)login {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"login = %@", login]];
    [request setFetchLimit:1];
    NSError *fetchError;
    int count = (int)[_managedObjectContext countForFetchRequest:request
                                                            error:&fetchError];
    BOOL isNotEmpty = count;
    return isNotEmpty;
}

- (void)compareAndUpgradeCompanyInfoAtDataBase:(NSString *)login {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"login = %@", login]];
    [request setFetchLimit:1];
    NSError *fetchError;
    Company *companyForUpgrade = [[self.managedObjectContext executeFetchRequest:request error:&fetchError] objectAtIndex:0];
    self.companyInfo = [[self.dataSource getCompanyInfo] mutableCopy];
//    NSTimeZone *serviceTime = [NSTimeZone timeZoneWithName:self.companyInfo[@"timezone"]];
//    NSLog (@"timezone %@", serviceTime);
    if (![self.companyInfo[@"name"] isEqual:[NSNull null]] && self.companyInfo[@"name"]) {
        if (![self.companyInfo[@"name"] isEqualToString:companyForUpgrade.companyName]) {
            [companyForUpgrade setCompanyName:self.companyInfo[@"name"]];
        }
    } else {
        [companyForUpgrade setCompanyName:@""];
    }
    
    if (![self.companyInfo[@"address1"] isEqual:[NSNull null]] && self.companyInfo[@"address1"]) {
        if (![self.companyInfo[@"address1"] isEqualToString:companyForUpgrade.addressOne]) {
            [companyForUpgrade setAddressOne:self.companyInfo[@"address1"]];
        }
    } else {
            [companyForUpgrade setAddressOne:@""];
    }
    if (![self.companyInfo[@"address2"] isEqual:[NSNull null]] && self.companyInfo[@"address2"]) {
        if (![self.companyInfo[@"address2"] isEqualToString:companyForUpgrade.addressTwo]) {
            [companyForUpgrade setAddressTwo:self.companyInfo[@"address2"]];
        }
    } else {
        [companyForUpgrade setAddressTwo:@""];
    }

    if (![self.companyInfo[@"city"] isEqual:[NSNull null]] && self.companyInfo[@"city"]) {
        if (![self.companyInfo[@"city"] isEqualToString:companyForUpgrade.city]) {
            [companyForUpgrade setCity:self.companyInfo[@"city"]];
        }
    } else {
       [companyForUpgrade setCity:@""];
    }
    
    if (![self.companyInfo[@"email"] isEqual:[NSNull null]] && self.companyInfo[@"email"]) {
        if (![self.companyInfo[@"email"] isEqualToString:companyForUpgrade.email]) {
            [companyForUpgrade setEmail:self.companyInfo[@"email"]];
        }
    } else {
        [companyForUpgrade setEmail:@""];
    }
    if (![self.companyInfo[@"phone"] isEqual:[NSNull null]] && self.companyInfo[@"phone"]) {
        if (![self.companyInfo[@"phone"] isEqualToString:companyForUpgrade.phone]) {
            [companyForUpgrade setPhone:self.companyInfo[@"phone"]];
        }
    } else {
        [companyForUpgrade setPhone:@""];
    }

    NSString *companyDescriptionInfo = self.companyInfo[@"description_text"];
    NSData *descriptionData = [NSKeyedArchiver archivedDataWithRootObject:companyDescriptionInfo];
    if (![descriptionData isEqualToData:companyForUpgrade.companyDescription]) {
//        NSLog(@"Data upgrated");
        [companyForUpgrade setCompanyDescription:descriptionData];
    }

    NSString *imagePathURL = self.companyInfo[@"logo"];
    if (![imagePathURL isEqual:[NSNull null]]) {
        UIImage *image = [self.dataSource pictureFormURL:self.companyInfo[@"logo"]];
        NSData *logoImageData = [NSKeyedArchiver archivedDataWithRootObject:image];
        if (![logoImageData isEqualToData:companyForUpgrade.logo]) {
            [companyForUpgrade setLogo:logoImageData];
        }
    } else {
        [companyForUpgrade setLogo:nil];
    }
    
    [self.application saveContext];
}

- (void)loadCompanyInfoFromServer {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.dataSource showProgressHUD];
        [self saveCompanyInfoToDataBase];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self showLabels];
            if (![self.companyInfo[@"logo"] isEqual:[NSNull null]]) {
                UIImage *image = [self.dataSource pictureFormURL:self.companyInfo[@"logo"]];
                self.logoImageView.image = image;
            }
            [_dataSource hideProgressHUD];
        });
    });
}

- (void)saveCompanyInfoToDataBase {
    self.companyInfo = [[self.dataSource getCompanyInfo] mutableCopy];
    self.company = (Company *) [NSEntityDescription
                                insertNewObjectForEntityForName:@"Company"
                                inManagedObjectContext:self.managedObjectContext];
    [self.company setLogin:self.companyLogin];
    [self.company setCompanyName:self.companyInfo[@"name"]];
    [self.company setFavorites:NO];
    if (![self.companyInfo[@"address1"] isEqual:[NSNull null]]) {
        [self.company setAddressOne:self.companyInfo[@"address1"]];
    }
    if (![self.companyInfo[@"address2"] isEqual:[NSNull null]]) {
        [self.company setAddressTwo:self.companyInfo[@"address2"]];
    }
    if (![self.companyInfo[@"city"] isEqual:[NSNull null]]) {
        [self.company setCity:self.companyInfo[@"city"]];
    }
    if (![self.companyInfo[@"email"] isEqual:[NSNull null]]) {
        [self.company setEmail:self.companyInfo[@"email"]];
    }
    if (![self.companyInfo[@"phone"] isEqual:[NSNull null]]) {
        [self.company setPhone:self.companyInfo[@"phone"]];
    }
    NSString *companyDescriptionInfo = self.companyInfo[@"description_text"];
    NSData *descriptionData = [NSKeyedArchiver archivedDataWithRootObject:companyDescriptionInfo];
    [self.company setCompanyDescription:descriptionData];
    NSString *imagePathURL = self.companyInfo[@"logo"];
    if (![imagePathURL isEqual:[NSNull null]] && imagePathURL) {
        UIImage *image = [self.dataSource pictureFormURL:self.companyInfo[@"logo"]];
        NSData *logoImageData = [NSKeyedArchiver archivedDataWithRootObject:image];
        // NSLog(@"set logo %@", self.companyInfo[@"logo"]);
        [self.company setLogo:logoImageData];
    }
//    else {
//        [self.company setLogo:nil];
//    }
//    NSLog(@"save data");
    [self.application saveContext];
}

- (void)loadCompanyInfoFromCoreData {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"login = %@", self.companyLogin]];
    [request setFetchLimit:1];
    
    NSError *fetchError;
    self.company = [[self.managedObjectContext executeFetchRequest:request error:&fetchError] objectAtIndex:0];
    [self.companyInfo setValue:self.company.addressOne forKey:@"address1"];
    [self.companyInfo setValue:self.company.addressTwo forKey:@"address2"];
    [self.companyInfo setValue:self.company.city forKey:@"city"];
    [self.companyInfo setValue:self.company.companyName forKey:@"name"];
    [self.companyInfo setValue:self.company.email forKey:@"email"];
    [self.companyInfo setValue:self.company.phone forKey:@"phone"];
    if (![self.company.companyDescription isEqual:[NSNull null]]) {
        NSString *companyDescription = [NSKeyedUnarchiver unarchiveObjectWithData:self.company.companyDescription];
        [self.companyInfo setValue:companyDescription forKey:@"description"];
    }
    if (![self.company.logo isEqual:[NSNull null]] && self.company.logo) {
        UIImage *companyLogo = [NSKeyedUnarchiver unarchiveObjectWithData:self.company.logo];
        self.logoImageView.image = companyLogo;
    }
    if (self.company.favorites) {
        [self.addFavoritesButton setImage:[UIImage imageNamed:@"starOn.png"] forState:UIControlStateNormal];
        self.favoritesLabel.text = @"Remove from favorites";
    } else {
        [self.addFavoritesButton setImage:[UIImage imageNamed:@"starOf-32.png"] forState:UIControlStateNormal];
        self.favoritesLabel.text = @"Add company to my favorites list";
    }
//    NSLog(@"self.companyInfo %@", self.companyInfo);
    [self showLabels];
}

- (void)clearLabels {
    self.addressOne.text = @"";
    self.city.text = @"";
    self.name.text = @"";
    self.email.text = @"";
    self.phoneNumber.text = @"";
    self.phone.text = @"";
    self.emailLabel.text = @"";
    self.logoImageView.hidden = YES;
    self.addFavoritesButton.hidden = YES;
    self.bookButton.hidden = YES;
    self.favoritesLabel.hidden = YES;
}

- (void)showLabels {
    if (self.companyInfo[@"address1"] != [NSNull null]) {
        self.addressOne.text = self.companyInfo[@"address1"];
    }
    if (self.companyInfo[@"city"]!= [NSNull null]) {
        self.city.text = self.companyInfo[@"city"];
    }
    if (self.companyInfo[@"name"] != [NSNull null] ) {
        self.name.text = self.companyInfo[@"name"];
    }
    if (self.companyInfo[@"email"] != [NSNull null]) {
        self.email.text = self.companyInfo[@"email"];
    }
    if (self.companyInfo[@"phone"]!= [NSNull null]) {
        self.phoneNumber.text = self.companyInfo[@"phone"];
    }
    if (self.companyInfo[@"description"]!= [NSNull null]) {
        NSString *html = self.companyInfo[@"description_text"];
        [self.webView loadHTMLString:html baseURL:nil];
    }
    self.logoImageView.hidden = NO;
    if (isCellBorder) {
        self.logoImageView.layer.borderWidth = 0.5f;
        self.logoImageView.layer.MasksToBounds = YES;
    }
    
    self.phone.text = @"Phone:";
    self.emailLabel.text = @"Email:";

    self.addFavoritesButton.hidden = NO;
    self.bookButton.hidden = NO;
    self.favoritesLabel.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)addedFavorites:(id)sender {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"login = %@", self.companyLogin]];
    NSError *fetchError;
    Company *company = [[self.managedObjectContext executeFetchRequest:request error:&fetchError] objectAtIndex:0];
    company.favorites = !company.favorites;
    if (company.favorites) {
        [self.addFavoritesButton setImage:[UIImage imageNamed:@"starOn.png"] forState:UIControlStateNormal];
        self.favoritesLabel.text = @"Remove from favorites";
    } else {
        [self.addFavoritesButton setImage:[UIImage imageNamed:@"starOf-32.png"] forState:UIControlStateNormal];
        self.favoritesLabel.text = @"Add company to my favorites list";
        
    }
    [company setFavorites:company.favorites];
    [self.application saveContext];
}

- (IBAction)bookButtonPressed:(id)sender {
//    NSLog(@"bookButtonPressed");
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
//    NSLog(@"start net checking");
    if (isNetwork) {
//    NSLog(@"isActivePluginCategory start checking");
//    BOOL isActivePluginCategory = [_dataSource isPluginActivated:serviceCategory];
        
//    NSLog(@"isActivePluginCategory stop checking");
        if (!isActivePluginCategory) {
            [self performSegueWithIdentifier:infoServiceSegue sender:self];
        } else {
//            NSLog(@"self.allCategory strart checking");
//            [self.dataSource showProgressHUD];
            
            self.allCategory = [_dataSource getCategoryList];
//            NSLog(@"self.allCategory stop checking");
            
            if ([self.allCategory count] == 0) {
                [self performSegueWithIdentifier:infoServiceSegue sender:self];
                self.serviceController.sendedListOfEvents = nil;
                
            } else {
                [self performSegueWithIdentifier:categorySegue sender:self];
                self.categoryController.allCategory = [self.allCategory copy];
//                NSLog(@"perfomr to category");
            }
        }
    }
}

- (IBAction)sharedPressed:(id)sender {
    NSString *textToShare = kMessageForShare;
    NSString *websiteString = [NSString stringWithFormat:@"https://%@.simplybook.me", self.companyLogin];
    NSURL *website = [NSURL URLWithString:websiteString];
    NSArray *objectsToShare = @[textToShare, website];
    UIActivityViewController *activity = [[UIActivityViewController alloc]
                                          initWithActivityItems:objectsToShare
                                          applicationActivities:nil];
    [self presentViewController:activity animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:categorySegue]) {
        self.categoryController = [segue destinationViewController];
    }
    if ([segue.identifier isEqualToString:infoServiceSegue]) {
        self.serviceController = [segue destinationViewController];
    }
}

@end
