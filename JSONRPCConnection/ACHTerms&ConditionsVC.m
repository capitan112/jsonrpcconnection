//
//  ACHTerms&ConditionsVC.m
//  SimplyBook
//
//  Created by Капитан on 09.12.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHTerms&ConditionsVC.h"

@interface ACHTerms_ConditionsVC ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
//@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation ACHTerms_ConditionsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"terms&Conditions" ofType:@"txt"];
    NSError *error;
    NSString *fileContents = [NSString stringWithContentsOfFile:filePath
                                                       encoding:NSUTF8StringEncoding
                                                          error:&error];
    if (error)
        NSLog(@"Error reading file: %@", error.localizedDescription);

    self.textView.text = fileContents;
    self.confirmButton.layer.cornerRadius = 5;
    self.confirmButton.layer.borderWidth = 1;
    self.confirmButton.layer.borderColor = self.confirmButton.tintColor.CGColor;
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"Terms & Conditions";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)confirmButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
