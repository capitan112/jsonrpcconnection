//
//  ACHTableViewControllerPerfomers.h
//  SimplyBookApp
//
//  Created by Капитан on 23.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACHTableViewControllerPerfomers : UITableViewController

@property (nonatomic, strong) NSArray *sendedPerfomers;
@property (nonatomic, strong) NSString *eventID;
@property (nonatomic, strong) NSDictionary *currentEventsList;
@property (nonatomic, strong) NSDictionary *fullPerformersList;

@end
