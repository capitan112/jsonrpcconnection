//
//  ACHCompanyWebPageVC.m
//  SimplyBookMobile 
//
//  Created by Капитан on 18.12.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHCompanyWebPageVC.h"

@interface ACHCompanyWebPageVC ()

@property (weak, nonatomic) IBOutlet UIWebView *viewWeb;

@end

@implementation ACHCompanyWebPageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *url = [NSURL URLWithString:self.fullURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.viewWeb loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
