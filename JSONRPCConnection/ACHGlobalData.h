//
//  ACHGlobalData.h
//  SimplyBook
//
//  Created by Капитан on 08.12.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kMessageForShare;
extern NSString *const secureKeyAPI;
extern BOOL const isCellBorder;

@interface ACHGlobalData : NSObject

@end
