//
//  ACHCompanyInfoViewController.h
//  SimplyBook-me
//
//  Created by Капитан on 13.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACHCompanyInfoViewController : UIViewController

@property (nonatomic, strong) NSString *companyLogin;

@end
