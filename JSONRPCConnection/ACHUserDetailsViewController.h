//
//  ACHUserDetailsViewController.h
//  SimplyBookApp
//
//  Created by Капитан on 02.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserDetail <NSObject>

- (void)userDetailsDictionary:(NSDictionary *)dict;

@end

@interface ACHUserDetailsViewController : UIViewController

@property (nonatomic, strong) NSMutableDictionary *dataReservation;
@property (nonatomic, strong) NSDictionary *fullPerformersList;

@property (nonatomic, retain) id <UserDetail> delegate;

@end
