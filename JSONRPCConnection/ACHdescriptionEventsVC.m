//
//  ACHdescriptionEventsVC.m
//  SimplyBook-me
//
//  Created by Капитан on 04.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHdescriptionEventsVC.h"
#import "ACHTableViewControllerPerfomers.h"
#import "ACHDataSource.h"
#import "ACHCompanyWebPageVC.h"

static NSString *const descriptionToPerfomerSegue = @"descriptionToPerfomerSegue";
static NSString *const serviceDescriptWebSegue = @"serviceDescriptWebSegue";


@interface ACHdescriptionEventsVC ()

@property (weak, nonatomic) IBOutlet UITextView *descriptionView;
@property (nonatomic, strong) ACHTableViewControllerPerfomers *destinationController;
@property (nonatomic, strong) ACHCompanyWebPageVC *webPageVC;
@property (nonatomic, strong) ACHDataSource *dataSource;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceDescription;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation ACHdescriptionEventsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataSource = [ACHDataSource sharedInstance];
    
    self.titleLabel.text = self.allEventsList[@"name"];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    self.descriptionView.text = _allEventsList[@"description"];
    if (![_allEventsList[@"description"] isEqual:[NSNull null]]) {
        self.descriptionView.text = _allEventsList[@"description"];
    } else {
        self.descriptionView.text = @"";
    }
    
    NSString *duration = self.currentEventsList[@"duration"];

    
    
    int minutesDuration = [duration intValue];
    NSString *timeDuration;
    if (minutesDuration >= 60) {
        int hours = minutesDuration / 60;
        minutesDuration = minutesDuration - hours * 60;
        if (minutesDuration > 0) {
            timeDuration = [NSString stringWithFormat:@"%dh %dmin", hours, minutesDuration];
        } else {
            timeDuration = [NSString stringWithFormat:@"%dh", hours];
        }
        
        
    } else {
        timeDuration = [NSString stringWithFormat:@"%dmin", minutesDuration];
    }
    NSString *isHideDurationString = self.currentEventsList[@"hide_duration"];
    NSString *priceFromDict = self.currentEventsList[@"price"];
    NSMutableString *serviceInfoText = [[NSMutableString alloc] init];
    NSString *priceString = nil;
    float priceFloat = 0;
    
    if (![priceFromDict isEqual:[NSNull null]]) {
        priceFloat = [priceFromDict floatValue];
        priceString = [NSString stringWithFormat:@"%.2f", priceFloat];
    }

    if (duration && [isHideDurationString isEqual:@"0"]) {
        [serviceInfoText appendFormat:@"%@ ", timeDuration];
    }
    
    if (priceString && priceFloat > 0) {
        NSString *currency = self.currentEventsList[@"currency"];
        [serviceInfoText appendFormat:@"%@ %@",currency, priceString];
    }
    
    if (serviceInfoText) {
        self.infoLabel.hidden = NO;
        self.serviceDescription.text = serviceInfoText;
    }
    
    if ([serviceInfoText isEqualToString:@""]) {
        self.infoLabel.hidden = YES;
    }
    
    if ([self.descriptionView.text isEqualToString:@""] || !self.descriptionView){
        self.descriptionLabel.hidden = YES;
    }
}

- (IBAction)selectedButton:(id)sender {
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
    if (isNetwork) {
        BOOL isPaymentRequired = [self.dataSource isPaymentRequired:self.eventID];
        if (isPaymentRequired) {
            [self performSegueWithIdentifier:serviceDescriptWebSegue sender:self];
            NSString *websiteString = [NSString stringWithFormat:@"https://%@.simplybook.me", [self.dataSource companyLogin]];
            self.webPageVC.fullURL = [websiteString copy];
        } else {
            [self performSegueWithIdentifier:descriptionToPerfomerSegue sender:self];
            self.destinationController.sendedPerfomers = [self.sendedPerfomers copy];
            self.destinationController.eventID = [self.eventID copy];
            self.destinationController.currentEventsList = [self.currentEventsList copy];
            self.destinationController.fullPerformersList = [self.fullPerformersList copy];
        }
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:descriptionToPerfomerSegue]) {
        self.destinationController = [segue destinationViewController];
    }
    if ([segue.identifier isEqualToString:serviceDescriptWebSegue]) {
        self.webPageVC = [segue destinationViewController];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
