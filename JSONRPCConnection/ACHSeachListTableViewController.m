//
//  ACHSeachListTableViewController.m
//  SimplyBook-me
//
//  Created by Капитан on 26.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHSeachListTableViewController.h"
//#import "ACHAppDelegate.h"
#import "Booking.h"
#import "Company.h"
#import "ACHDataSource.h"
#import "ACHGlobalData.h"


static NSString *const reuseCellIdentifier = @"listCell";
static NSString *const companyInfoSegue = @"infoSegue";
static NSString *const mapSegue = @"mapSegue";
static int rowsAtRequestToServer = 10;
static int defaultRowsAtRequestToServer = 50;

static const int rowHeight = 110;
static const int lastRowHeight = 30;

@interface ACHSeachListTableViewController () {
    NSArray *searchResults;
}

//@property (nonatomic, strong) ACHAppDelegate *application;
@property (nonatomic, strong) ACHDataSource *dataSource;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) NSMutableArray *companyList;
@property (nonatomic, copy) NSString *seachingWord;
@property (nonatomic, assign) int startRow;
@property (nonatomic, assign) BOOL isLastObject;
@property (nonatomic, strong)  UIView *footerView;
@property (nonatomic, strong) UIView *tableFooterView;
@property (nonatomic, assign) BOOL isLocationGetting;

@end

@implementation ACHSeachListTableViewController

@synthesize tableFooterView;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = [ACHDataSource sharedInstance];
    [self startStandardUpdates];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self addTableViewFooter];
    self.isLocationGetting = FALSE;
    
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
//    label.backgroundColor = [UIColor clearColor];
//    label.textColor = [UIColor greenColor]; // change this color
//    self.navigationItem.titleView = label;
//    label.text = @"Confirmed";
//    [label sizeToFit];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:YES];
    [self.locationManager stopUpdatingLocation];
}

# pragma mark - Add footer

- (void)addTableViewFooter {
    tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 80)];
    [tableFooterView setBackgroundColor:[UIColor clearColor]];
    UIButton *inviteCompanyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [inviteCompanyButton setTitle:@"Invite company" forState:UIControlStateNormal];
    [inviteCompanyButton addTarget:self action:@selector(inviteCompany:) forControlEvents:UIControlEventTouchUpInside];
    [inviteCompanyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [inviteCompanyButton setBackgroundColor:inviteCompanyButton.tintColor];
//    inviteCompanyButton.frame = CGRectMake(0, 0, 304, 47);
    inviteCompanyButton.layer.cornerRadius = 5;
    inviteCompanyButton.layer.borderWidth = 1;
    inviteCompanyButton.layer.borderColor = inviteCompanyButton.tintColor.CGColor;
    [tableFooterView addSubview:inviteCompanyButton];
    inviteCompanyButton.translatesAutoresizingMaskIntoConstraints = NO;

//    NSLayoutConstraint *height =
//    [NSLayoutConstraint constraintWithItem:inviteCompanyButton
//                                 attribute:NSLayoutAttributeHeight
//                                 relatedBy:0
//                                    toItem:tableFooterView
//                                 attribute:NSLayoutAttributeHeight
//                                multiplier:1.0
//                                  constant:-40];
//
//    [tableFooterView addConstraint:height];
//    NSLayoutConstraint *right =
//    [NSLayoutConstraint constraintWithItem:inviteCompanyButton
//                                 attribute:NSLayoutAttributeRight
//                                 relatedBy:0
//                                    toItem:tableFooterView
//                                 attribute:NSLayoutAttributeRight
//                                multiplier:1.0
//                                  constant:-8];
//    
//    [tableFooterView addConstraint:right];
//    NSLayoutConstraint *left =
//            [NSLayoutConstraint constraintWithItem:inviteCompanyButton
//                                         attribute:NSLayoutAttributeLeft
//                                         relatedBy:0
//                                            toItem:tableFooterView
//                                         attribute:NSLayoutAttributeLeft
//                                        multiplier:1.0
//                                          constant:8];
//    [tableFooterView addConstraint:left];
//    NSLayoutConstraint *top =
//            [NSLayoutConstraint constraintWithItem:inviteCompanyButton
//                                         attribute:NSLayoutAttributeTop
//                                         relatedBy:0
//                                            toItem:tableFooterView
//                                         attribute:NSLayoutAttributeTop
//                                        multiplier:1.0
//                                          constant:8];
//    [tableFooterView addConstraint:top];
    
//    inviteCompanyButton.center = inviteCompanyButton.superview.center;

    NSDictionary *views = @{ @"view" : tableFooterView,  @"button" : inviteCompanyButton } ;
    
    [tableFooterView addConstraints:
                    [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[button]-|"
                                                            options:0
                                                            metrics:nil
                                                              views:views]];
   [tableFooterView addConstraints:
                [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[inviteCompanyButton(44)]"
                                                        options:0
                                                        metrics:nil
                                                          views:NSDictionaryOfVariableBindings( inviteCompanyButton)]];
    
    
    [self.tableView setTableFooterView:tableFooterView];
}


# pragma mark - SearchDisplayController delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
    if (!isNetwork) {
        self.dataSource = nil;
        self.companyList = nil;
        [self refreshTableView];
    } else {
        [self seachWord:searchBar.text withRows:rowsAtRequestToServer];
    }
}

//- (void)startSearchingService {
//    [self seachWord:@"" withRows:defaultRowsAtRequestToServer];
//}

- (void)seachWord:(NSString *)searchWord withRows:(int)rows {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        self.dataSource = [ACHDataSource sharedInstance];
        [self.dataSource showProgressHUD];
        NSArray *dataFromServer = [self searchServicesWithContainsWord:searchWord
                                                              startRow:0
                                                         rowsAtRequest:rows];
        _seachingWord = searchWord;
        self.companyList = [[NSMutableArray alloc] initWithArray:dataFromServer];
        _isLastObject = NO;
        _startRow = 0;
        NSInteger loadedRows = [dataFromServer count];
        if (!(loadedRows == rowsAtRequestToServer))
            _isLastObject = YES;
        [self refreshTableView];
    });
}

- (NSArray *)searchServicesWithContainsWord:(NSString *)containsWord startRow:(int)startRow rowsAtRequest:(int)rowsAtRequest {
    NSArray *companyList;
    if (_currentLocation) {
//        NSLog(@"_currentLocation %@", _currentLocation);
        [_dataSource showProgressHUD];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        int seachingDistance = [[defaults objectForKey:@"seachingDistance"] intValue];
        if (seachingDistance <= 1) {
            [defaults setValue:[NSNumber numberWithFloat:12]  forKey:@"seachingDistance"];
            [defaults synchronize];
            seachingDistance = 12;
        }
        companyList = [self.dataSource companyiesWithContainsWord:containsWord
                                                            seachingRadius:seachingDistance
                                                           currentLatitude:
                                _currentLocation.coordinate.latitude
                                                          currentLongitude:
                                _currentLocation.coordinate.longitude
                                                                  startRow:startRow
                                                      requestRows:rowsAtRequest];
        [_dataSource hideProgressHUD];
    }

    return [companyList copy];
}

- (void)refreshTableView {
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self.searchDisplayController setActive:NO animated: YES];
        [self.tableView reloadData];
        [_dataSource hideProgressHUD];
    });
}

# pragma mark - CoreLocation delegate

- (void)startStandardUpdates {
    [self.locationManager requestWhenInUseAuthorization];
    if (_locationManager == nil) {
        _locationManager = [[CLLocationManager alloc] init];
    }
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    if ([CLLocationManager  locationServicesEnabled]) {
        if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [_locationManager requestWhenInUseAuthorization];
        }
        [_locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    _currentLocation = [locations lastObject];
    
    if (!self.isLocationGetting && _currentLocation){
        self.isLocationGetting = TRUE;
        [self seachWord:@"" withRows:defaultRowsAtRequestToServer];
    }
    
//    NSLog(@"latitude %+.6f, longitude %+.6f\n",
//          _currentLocation.coordinate.latitude,
//          _currentLocation.coordinate.longitude);

}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"error %@", error);
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return [self.companyList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseCellIdentifier];
    }
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    cell.textLabel.minimumScaleFactor = 0.5;
    cell.textLabel.text  = self.companyList[indexPath.row][@"name"];
    cell.detailTextLabel.text = self.companyList[indexPath.row][@"description_text"];
    cell.imageView.backgroundColor = [UIColor whiteColor];
    cell.imageView.image = [UIImage imageNamed:@"noPhotoCategory.png"];
    
    NSString *pictureURL = self.companyList[indexPath.row][@"logo"];
      if (![pictureURL isEqual:[NSNull null]]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            cell.imageView.backgroundColor = [UIColor whiteColor];
            UIImage *image = [_dataSource pictureFormURL:pictureURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.imageView.image = image;
            });
        });

    }
    
    if (isCellBorder) {
        cell.imageView.layer.borderWidth = 0.5f;
        cell.imageView.layer.MasksToBounds = YES;
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger totalRow = [self.companyList count];
    if (indexPath.row == totalRow)
        return lastRowHeight;
    return rowHeight;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger totalRow = [self.companyList count];
    if (!(indexPath.row == totalRow)) {
        [self.dataSource showProgressHUD];
        [self performSegueWithIdentifier:companyInfoSegue sender:nil];
        self.infoController.companyLogin = self.companyList[indexPath.row][@"login"];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height) {
//        NSLog(@"dataFromServer %d", [dataFromServer count]);
        if (!_isLastObject) {
            [self.dataSource showProgressHUD];
            _startRow += rowsAtRequestToServer;
             NSArray *dataFromServer = [self searchServicesWithContainsWord:_seachingWord
                                                                   startRow:_startRow
                                                              rowsAtRequest:rowsAtRequestToServer];
            NSInteger rowsAtArray = [dataFromServer count];
            if (rowsAtArray == 0){
                _isLastObject = YES;
                [self.dataSource hideProgressHUD];
                return;
            } else if (rowsAtArray < rowsAtRequestToServer) {
                _isLastObject = YES;
            }
            [self.companyList addObjectsFromArray:dataFromServer];
            [self.dataSource hideProgressHUD];
            [self.tableView reloadData];
        }
    }
}


- (void)inviteCompany:(id)sender {
    NSString *textToShare = kMessageForShare;
    NSURL *website = [NSURL URLWithString:@"https://simplybook.me"];
    NSArray *objectsToShare = @[textToShare, website];
    
    UIActivityViewController *activity = [[UIActivityViewController alloc]
                                          initWithActivityItems:objectsToShare
                                          applicationActivities:nil];
    [self presentViewController:activity animated:YES completion:nil];

}

#pragma mark - Segue Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:companyInfoSegue]) {
        self.infoController = [segue destinationViewController];
    }
    if ([[segue identifier] isEqualToString: mapSegue]) {
        self.mapController = [segue destinationViewController];
        self.mapController.currentLocation = self.currentLocation;
        self.mapController.companyList = self.companyList;
    }
}

- (IBAction)mapLoaded:(id)sender {
    [self performSegueWithIdentifier:mapSegue sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
