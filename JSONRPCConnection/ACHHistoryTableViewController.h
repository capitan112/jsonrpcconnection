//
//  ACHHistoryTableViewController.h
//  SimplyBook-me
//
//  Created by Капитан on 17.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ACHHistoryDetailViewController.h"

@interface ACHHistoryTableViewController : UITableViewController <NSFetchedResultsControllerDelegate, ACHHistoryDetailViewControllerDelegate >

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end
