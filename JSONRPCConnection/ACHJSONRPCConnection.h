//
//  CAJSONRPCConnection.h
//  JSONRPCConnection
//
//  Created by Капитан on 04.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, sourceURL) {
    registration,
    catalog,
    user
};

@interface ACHJSONRPCConnection : NSObject {
    enum sourceURL catalogURL;
}

//+ (id)sharedInstanceWithLogin:(NSString *)companyLogin;
- (id)initWithCompanyLogin:(NSString *)companyLogin;
- (NSDictionary *)requestWithDict:(NSDictionary *)dict fromURL:(enum sourceURL)URLEnum;
    
@end
