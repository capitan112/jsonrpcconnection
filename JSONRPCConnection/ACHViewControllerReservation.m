//
//  ACHViewControllerReservation.m
//  SimplyBook-me
//
//  Created by Капитан on 08.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//userSelectedData

#import "ACHViewControllerReservation.h"
#import "ACHDataSource.h"
#import "ACHUserDetailsViewController.h"
#import "ACHPickerViewController.h"
#import "ACHTextFiledViewController.h"
#import "ACHTextViewController.h"
#import "ACHOrderConfimationViewController.h"

#import "ACHAppDelegate.h"
#import "Booking.h"
#import "Company.h"


#define kHeaderViewTag 5
#define kDateIconTag 10
#define kServiceIconTag 20
#define kPerfomerIconTag 30

#define kDateLabelTag 40
#define kServiceLabelTag 50
#define kPerfomerLabelTag 60
#define kSwitchTag 70
#define kFooterViewTag 100
#define kConfimButton 300

static NSString *const reuseCellIdentifier = @"reservationCell";
static NSString *const headerCellIdentifier = @"headerCell";
static NSString *const footerCellIdentifier = @"footerCell";
static NSString *const switchCellIdentifier = @"switchCell";
static NSString *const userDetailsKey = @"userDetailKey";
static NSString *const selectedDictKey = @"selectedData";
static NSString *const userDetailSegue = @"userDetailSegue";
static NSString *const pickerSegue = @"pickerSegue";
static NSString *const textFieldSegue = @"textFieldSegue";
static NSString *const textViewSegue = @"textViewSegue";
static NSString *const bookReportSegue = @"bookReportSegue";

static int const headerHeight = 70;
static int const footerHeight = 120;

@interface ACHViewControllerReservation () {
    UIButton *confirmButton;
}

@property (nonatomic, strong) ACHDataSource *dataSource;
@property (nonatomic, strong) NSArray *additionalFields;
@property (nonatomic, strong) ACHUserDetailsViewController *userDetailController;
@property (nonatomic, strong) ACHPickerViewController *pickerController;
@property (nonatomic, strong) ACHTextFiledViewController *textFieldController;
@property (nonatomic, strong) ACHTextViewController *textViewController;
@property (nonatomic, strong) ACHOrderConfimationViewController *bookReportController;
@property (strong, nonatomic) NSMutableDictionary *userSelectedData;
@property (strong, nonatomic) NSDictionary *bookingFormServer;
@property (strong, nonatomic) ACHAppDelegate *application;
@property (strong, nonatomic) NSDictionary *companyInfo;
@property (strong, nonatomic) NSMutableDictionary *mandatoryFields;
@property (strong, nonatomic) NSUserDefaults *defaults;
@property (weak, nonatomic) IBOutlet UILabel *userDetailLabel;

@end

@implementation ACHViewControllerReservation
@synthesize application;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.defaults = [NSUserDefaults standardUserDefaults];
    self.dataSource = [ACHDataSource sharedInstance];

    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self loadAdditionalFields];
    NSData *dictionaryData = [self.defaults objectForKey:selectedDictKey];
    if (dictionaryData) {
        self.userSelectedData = [[NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData] mutableCopy];
    } else {
        self.userSelectedData = [[NSMutableDictionary alloc] init];
    }
    NSString *perfomerID = _dataReservation[@"perfomerID"];
    if ([perfomerID isEqualToString:@""]){
        NSString *reservationDate = [NSString stringWithFormat:@"%@",
                                                    _dataReservation[@"requiredDay"]];
        NSString *reservationTime = [NSString stringWithFormat:@"%@",
                                                    _dataReservation[@"requiredTime"]];
        NSString *eventID = _dataReservation[@"eventID"];
        NSArray *perfomersID = [_dataSource availableUnitsForEvent:eventID
                                                              date:reservationDate
                                                              time:reservationTime
                                                             count:@"1"];
        if ([perfomersID count] != 0) {
            NSString *perfomersIDkey = [NSString stringWithFormat:@"%@", perfomersID[0]];
            NSString *perfomerName = self.fullPerformersList[perfomersIDkey][@"name"];
            [_dataReservation setObject:perfomersIDkey forKey:@"perfomerID"];
            [_dataReservation setObject:perfomerName forKey:@"perfomerName"];
//           NSLog(@"perfomersID %@", perfomersIDkey);
        }
        [self.dataSource hideProgressHUD];
    }
    application = (ACHAppDelegate *)[[UIApplication sharedApplication]delegate];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [_dataSource hideProgressHUD];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable)
                                                 name:@"loadAdditionalFields"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(booked)
                                                 name:@"bookedResultLoaded"
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self userDetailsLoad];
    self.userDetailLabel.text = self.userDetails[@"name"];
    [self.tableView reloadData];
    confirmButton.enabled = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Loading data source

- (void)userDetailsLoad {
    NSData *userDetailDictionaryData = [self.defaults objectForKey:userDetailsKey];
    if (userDetailDictionaryData) {
        self.userDetails = [[NSKeyedUnarchiver unarchiveObjectWithData:userDetailDictionaryData] mutableCopy];
    } else {
        self.userDetails = [[NSMutableDictionary alloc] init];
    }
}

- (void)loadAdditionalFields {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [_dataSource showProgressHUD];
        BOOL isAdditionalFileds = [_dataSource isPluginActivated:@"event_field"];
//        NSLog(isAdditionalFileds ? @"additional fields: Yes" : @"additional fields: No");

        if (isAdditionalFileds) {
            self.additionalFields = [_dataSource additionalFields:_dataReservation[@"eventID"]];
//            NSLog(@"_additionalFields %@", self.additionalFields);
            self.mandatoryFields = [[NSMutableDictionary alloc] init];
            for (NSDictionary *item in self.additionalFields) {
                if ([item[@"is_null"] isEqual:[NSNull null]]){
                    [self.mandatoryFields setValue:item[@"is_null"] forKey:item[@"name"]];
                }
            }
//            NSLog(@"_additionalFields %@", self.mandatoryFields);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAdditionalFields"
                                                                object:self];
        }
    });
}

#pragma mark - Table view header

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:headerCellIdentifier];
    if (headerView == nil) {
        headerView = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:footerCellIdentifier];
    }
    UILabel *dateLabel = (UILabel *)[headerView viewWithTag:kDateLabelTag];
    UILabel *serviceLabel = (UILabel *)[headerView viewWithTag:kServiceLabelTag ];
    UILabel *perfomerLabel = (UILabel *)[headerView viewWithTag:kPerfomerLabelTag];
    

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    NSDate *reservationDate = _dataReservation[@"bookedDateAndTime"];
    NSString *reservationTime = [dateFormatter stringFromDate:reservationDate];

//    NSString *reservationTime = [NSString stringWithFormat:@"%@ %@ %@",
//                                      _dataReservation[@"requiredDay"],
//                                                               @" at ",
//                                     _dataReservation[@"requiredTime"]
//                                 ];
    
    
    dateLabel.text = reservationTime;
    serviceLabel.text = _dataReservation[@"eventName"];
    perfomerLabel.text = _dataReservation[@"perfomerName"];
    UIView *view = (UIView *)[headerView viewWithTag:kHeaderViewTag];
    return view;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return headerHeight;
}

#pragma mark - Table view footer

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UITableViewCell *footerView = [tableView dequeueReusableCellWithIdentifier:footerCellIdentifier];
    if (footerView == nil) {
        footerView = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:footerCellIdentifier];
    }
    UIView *view = (UIView *)[footerView viewWithTag:kFooterViewTag];    
    confirmButton = (UIButton *)[footerView viewWithTag:kConfimButton];
    confirmButton.layer.cornerRadius = 5;
    confirmButton.layer.borderWidth = 1;
    confirmButton.layer.borderColor = confirmButton.tintColor.CGColor;
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return footerHeight;
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_additionalFields count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    NSString *typeOfFields = self.additionalFields[indexPath.row][@"type"];

    if ([typeOfFields isEqualToString:@"checkbox"]){
        cell = [self createSwitchCell:indexPath];
    } else {
        cell = [self createHeaderCell:indexPath];
    }

    return cell;
}

- (UITableViewCell *)createHeaderCell:(NSIndexPath *)indexPath {
    UITableViewCell *headerCell = [self.tableView dequeueReusableCellWithIdentifier:reuseCellIdentifier];
    if (headerCell == nil) {
        headerCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseCellIdentifier];
    }
    NSString *nameOfFields = self.additionalFields[indexPath.row][@"title"];
    NSString *typeOfFields = self.additionalFields[indexPath.row][@"type"];
    NSString *keyOfFields = self.additionalFields[indexPath.row][@"name"];
    NSString *saveDataValue = [self.userSelectedData objectForKey:keyOfFields];
    if ([saveDataValue isKindOfClass:[NSNull class]] ) {
        saveDataValue = @"empty value";
    }
    NSString *mandatoryValue = self.additionalFields[indexPath.row][@"is_null"];
//  NSLog(@"mandatoryValue %@", mandatoryValue);
    if (!mandatoryValue || [mandatoryValue isEqual:[NSNull null]]) {
        NSString *titleOfFields = [NSString stringWithFormat:@"*%@", nameOfFields];
        headerCell.textLabel.text = titleOfFields;
//        NSLog(@"if saveDataValue %@", saveDataValue);
        if ([saveDataValue isEqualToString:@""] || !saveDataValue) {
            headerCell.textLabel.textColor = [UIColor redColor];
        } else {
            headerCell.textLabel.textColor = [UIColor blackColor];
        }
        
        
    } else {
        headerCell.textLabel.text = nameOfFields;
    }
    if (![typeOfFields isEqualToString:@"checkbox"]) {
        headerCell.detailTextLabel.text = saveDataValue;
    }
    
    return headerCell;
}

- (UITableViewCell *)createSwitchCell:(NSIndexPath *)indexPath {
    UITableViewCell *switchCell = [self.tableView dequeueReusableCellWithIdentifier:switchCellIdentifier];
    if (switchCell == nil) {
        switchCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:switchCellIdentifier];
    }
    
    UISwitch *cellSwitch = (UISwitch *)[switchCell viewWithTag:kSwitchTag];
    cellSwitch.tag = indexPath.row;
    NSString *titleOfFields = self.additionalFields[indexPath.row][@"title"];
    NSString *nameOfFields = self.additionalFields[indexPath.row][@"name"];
    
    if ([self.userSelectedData[nameOfFields] isKindOfClass:[NSNull class]] || [self.userSelectedData[nameOfFields] isEqualToString:@"on"]) {
        [cellSwitch setOn: YES];
        self.userSelectedData[nameOfFields] = @"on";
    } else {
        [cellSwitch setOn: NO];
        self.userSelectedData[nameOfFields] = @"off";
    }
    switchCell.textLabel.text = titleOfFields;
    return switchCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *typeOfFields = _additionalFields[indexPath.row][@"type"];
    NSString *keyOfFields = _additionalFields[indexPath.row][@"name"];
   
    if ([typeOfFields isEqualToString:@"select"] ) {
        [self performSegueWithIdentifier:pickerSegue sender:nil];
        self.pickerController.dataPickerSource = [_additionalFields[indexPath.row] copy];
        self.pickerController.selectedValue = self.userSelectedData[keyOfFields];
    } else if ([typeOfFields isEqualToString:@"text"] || [typeOfFields isEqualToString:@"digits"]) {
        [self performSegueWithIdentifier:textFieldSegue sender:nil];
        self.textFieldController.keyOfField = self.additionalFields[indexPath.row][@"name"];
        self.textFieldController.selectedValue = self.userSelectedData[keyOfFields];
        self.textFieldController.typeOfFields = typeOfFields;
    } else if ([typeOfFields isEqualToString:@"textarea"] ) {
        [self performSegueWithIdentifier:textViewSegue sender:nil];
        self.textViewController.keyOfField = self.additionalFields[indexPath.row][@"name"];
        self.textViewController.selectedValue = self.userSelectedData[keyOfFields];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:userDetailSegue]) {
        self.userDetailController = [segue destinationViewController];
        self.userDetailController.delegate = self;
    } else if ([segue.identifier isEqualToString:pickerSegue]) {
        self.pickerController = [segue destinationViewController];
        self.pickerController.delegate = self;
    } else if ([segue.identifier isEqualToString:textFieldSegue]) {
        self.textFieldController = [segue destinationViewController];
        self.textFieldController.delegate = self;
    } else if ([segue.identifier isEqualToString:textViewSegue]) {
        self.textViewController = [segue destinationViewController];
        self.textViewController.delegate = self;
    } else if ([segue.identifier isEqualToString:bookReportSegue]) {
        self.bookReportController = [segue destinationViewController];
    }
}

#pragma mark - Reload table

- (void)reloadTable {
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self saveUserDefaults];
        [self.tableView reloadData];
        [self.dataSource hideProgressHUD];
    });
}

#pragma mark - Go to User Details

- (IBAction)userDetailPressed:(id)sender {
    [self performSegueWithIdentifier:userDetailSegue sender:nil];
}

#pragma mark - Delegate User Details

- (void)selectedValue:(NSString *)value forKey:(NSString *)key {
    if (value) {
        [self.userSelectedData setObject:value forKey:key];
        [self saveUserDefaults];
    }
}

- (void)userDetailsDictionary:(NSDictionary *)dict {
    _userDetails = [dict mutableCopy];
}

#pragma mark - Other methods

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - save standardUserDefaults data

- (void)saveUserDefaults {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.userSelectedData];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:selectedDictKey];
    [self.defaults synchronize];
}

#pragma mark - switch methods

- (IBAction)changeSwitchValue:(id)sender {
    UISwitch *switcher = (UISwitch*)sender;
    NSString *switchValue;
    if (switcher.isOn){
        switchValue = @"on";
    } else {
        switchValue = @"off";
    }
    [self.userSelectedData setValue:switchValue forKey:_additionalFields[switcher.tag][@"name"]];
    [self saveUserDefaults];
}

#pragma mark - booked button

- (NSString *)timeOffset {
    NSTimeZone *serviceTime = [NSTimeZone timeZoneWithName:self.companyInfo[@"timezone"]];
    NSTimeZone *localTime = [NSTimeZone localTimeZone];
    NSDate *localDateNow = [NSDate date];
    NSInteger sourceGMTOffset = [localTime secondsFromGMTForDate:localDateNow];
    NSInteger destinationGMTOffset = [serviceTime secondsFromGMTForDate:localDateNow];
    NSTimeInterval interval =  sourceGMTOffset - destinationGMTOffset;
    
    return [[NSString stringWithFormat:@"%0.f", interval/60] copy];
    
}

- (IBAction)confirmReservation:(id)sender {
    for (NSString *item in [_userDetails allKeys]) {
        if ([_userDetails[item] length] == 0) {
            UIAlertView *alert =[[UIAlertView alloc ] initWithTitle:@"No user details"
                                                            message:@"Please fill user details"
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles: nil];
            [alert show];
            return;
        }
    }

    if ([self.dataReservation[@"perfomerID"] isEqualToString:@""]) {
        UIAlertView *alert =[[UIAlertView alloc ] initWithTitle:@"No booking available"
                                                        message:@"No performers. Please call to service"
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }

    for (NSString *key in self.mandatoryFields) {
        NSString *value = self.userSelectedData[key];
        if (!value || [value isEqualToString:@""]) {
//            NSLog(@"value %@", value);
            UIAlertView *alert =[[UIAlertView alloc ] initWithTitle:@"Field required to fill"
                                                            message:@"Please fill required fields"
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles: nil];
            [alert show];
            return;
        }

    }
    self.userDetails[@"device_type"] = @"apple";
    
    if (application.deviceToken) {
        self.userDetails[@"device_token"] = application.deviceToken;
    }
//    NSLog(@"confirmButton click");
    confirmButton.enabled = NO;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [_dataSource showProgressHUD];
        
        self.companyInfo = [self.dataSource getCompanyInfo];
        NSString *timeIntervalString;
        timeIntervalString = [self timeOffset];
        self.userDetails[@"client_time_offset"] = timeIntervalString;
        
        
        self.bookingFormServer =
                              [_dataSource bookWithEvent:self.dataReservation[@"eventID"]
                                                   unitId:self.dataReservation[@"perfomerID"]
                                              requiredDay:self.dataReservation[@"requiredDay"]
                                           requiredTime:self.dataReservation[@"requiredTime"]
                                              userDetails:[self.userDetails copy]
                                         additionalFields:[self.userSelectedData copy]];

//        NSLog(@"self.bookingFormServer %@", self.bookingFormServer);
        if (self.bookingFormServer) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"bookedResultLoaded" object:self];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loadAdditionalFields" object:self];
        }
    });
}

- (void)booked {
    [self.dataSource hideProgressHUD];
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self saveBookingInfo];
        [self performSegueWithIdentifier:bookReportSegue sender:nil];
        self.bookReportController.dataOfBooking = self.dataReservation;
        self.bookReportController.userDetails = [self.userDetails copy];
        self.bookReportController.userSelectedData = [self.userSelectedData copy];
        self.bookReportController.additionalFields = self.additionalFields;
        self.bookReportController.bookingFormServer = [self.bookingFormServer copy];
    
        [self saveUserDefaults];
    });
}

#pragma mark - Core data save booking

- (void)saveBookingInfo {
    NSDictionary *bookingDictionary = [self.bookingFormServer[@"result"][@"bookings"][0] copy];
//    application = (ACHAppDelegate *) [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *managedObjectContext = application.managedObjectContext;
    NSString *companyLogin = self.companyInfo[@"login"];
//    NSDictionary *companyInfo = [self.dataSource getCompanyInfo];
//    NSString *companyLogin = companyInfo[@"login"];

    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Company"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"login = %@", companyLogin]];
    [request setFetchLimit:1];

    NSError *fetchError;
    Company *company;
    NSUInteger count = [managedObjectContext countForFetchRequest:request error:&fetchError];
    if (count == NSNotFound) {
        NSLog(@"Error %@", fetchError);
    } else if (count == 0) {
        company = (Company *) [NSEntityDescription insertNewObjectForEntityForName:@"Company"
                                                            inManagedObjectContext:managedObjectContext];
       [company setLogin:companyLogin];
    } else {
        company = [[managedObjectContext executeFetchRequest:request error:&fetchError] objectAtIndex:0];
    }
    
    Booking *history = (Booking *)[NSEntityDescription insertNewObjectForEntityForName:@"Booking" inManagedObjectContext:managedObjectContext];
    [history setPerfomerName:_dataReservation[@"perfomerName"]];
    [history setServiceName:_dataReservation[@"eventName"]];
    [history setCode:bookingDictionary[@"code"]];
    [history setBookingHash:bookingDictionary[@"hash"]];
    NSString *reservedDate = [NSString stringWithFormat:@"%@ %@",self.dataReservation[@"requiredDay"], self.dataReservation[@"requiredTime"]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    [history setStartDateTime:[dateFormatter dateFromString:reservedDate]];
    NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc] init];
    [numberFormat setNumberStyle:NSNumberFormatterDecimalStyle];
    [history setBookingId:[numberFormat numberFromString:bookingDictionary[@"id"]]];
    
    NSDictionary *selectedAdditionalField = [self convertAdditionalFields];
//    NSLog(@"selectedAdditionalField %@", selectedAdditionalField);
    
    
    NSData *additionalFieldsData = [NSKeyedArchiver archivedDataWithRootObject:selectedAdditionalField];
    [history setAdditionalFields:additionalFieldsData];
    [company addBookingObject:history];
    NSError *error = nil;
    
    if (![managedObjectContext save:&error]) {
        NSLog(@"Error! %@", error);
    }
}

#pragma mark - button pressed

- (IBAction)cancelBooking:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (NSDictionary *)convertAdditionalFields {
    NSMutableDictionary *selectedUserAdditionalField = [[NSMutableDictionary alloc] init];
    int arraySize = (int)[self.additionalFields count];

    for (NSString *key in [self.userSelectedData allKeys]) {
        for (int i = 0; i < arraySize; i++) {
            if ([self.additionalFields[i][@"name"] isEqualToString:key]){
                [selectedUserAdditionalField setValue:self.userSelectedData[key] forKey:self.additionalFields[i][@"title"]];
            }
        }
    }
    
    return [selectedUserAdditionalField mutableCopy];
}


@end
