//
//  ACHdescriptionEventsVC.h
//  SimplyBook-me
//
//  Created by Капитан on 04.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACHdescriptionEventsVC : UIViewController

@property (nonatomic, strong) NSDictionary *allEventsList;
@property (nonatomic, strong) NSArray *sendedPerfomers;
@property (nonatomic, strong) NSString *eventID;
@property (nonatomic, strong) NSDictionary *currentEventsList;
@property (nonatomic, strong) NSString *serviceInfoText;
@property (nonatomic, strong) NSDictionary *fullPerformersList;

@end
