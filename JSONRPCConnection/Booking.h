//
//  Booking.h
//  SimplyBook-me
//
//  Created by Капитан on 23.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Company;

@interface Booking : NSManagedObject

@property (nonatomic, retain) NSData * additionalFields;
@property (nonatomic, retain) NSString * bookingHash;
@property (nonatomic, retain) NSNumber * bookingId;
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSString * perfomerName;
@property (nonatomic, retain) NSString * serviceName;
@property (nonatomic, retain) NSDate * startDateTime;
@property (nonatomic, retain) Company *companyName;

@end
