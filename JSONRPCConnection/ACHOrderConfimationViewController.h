//
//  ACHOrderConfimationViewController.h
//  SimplyBook-me
//
//  Created by Капитан on 10.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface ACHOrderConfimationViewController : UIViewController

@property (nonatomic, strong) NSDictionary *dataOfBooking;
@property (nonatomic, strong) NSDictionary *userDetails;
@property (nonatomic, strong) NSDictionary *userSelectedData;
@property (nonatomic, strong) NSArray *additionalFields;
@property (nonatomic, strong) NSDictionary *bookingFormServer;

@end
