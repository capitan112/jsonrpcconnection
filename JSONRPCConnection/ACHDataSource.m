//
//  CADataSource.m
//  JSONRPCConnection
//
//  Created by Капитан on 04.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//


#import <CommonCrypto/CommonDigest.h>
#import "ACHDataSource.h"
#import "ACHJSONRPCConnection.h"
#import "ProgressHUD.h"
#import "Reachability.h"
#import "ACHGlobalData.h"

static const BOOL debugTools = NO;

@interface ACHDataSource ()

@property (nonatomic, strong) ACHJSONRPCConnection *connection;
@property (nonatomic) NSInteger unitArraySize;
@property (nonatomic, strong) NSCache *cache;
@property (nonatomic, strong) NSDictionary *companyInfo;

@end

@implementation ACHDataSource

static ACHJSONRPCConnection *sharedInstance = nil;

#pragma mark class methods

+ (id)sharedInstance {
    return [self sharedInstanceWithLogin:nil];
}

//+ (id)sharedInstanceWithLogin:(NSString *)companyLogin keyAPI:(NSString *)keyAPI {
//    static ACHJSONRPCConnection *sharedInstance = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        sharedInstance = [[self alloc] initWithLogin:companyLogin keyAPI:keyAPI];
//    });
//    
//    return sharedInstance;
//}

+ (id)sharedInstanceWithLogin:(NSString *)companyLogin {
    if (sharedInstance == nil) {
        sharedInstance = [[self alloc] initWithLogin:companyLogin];
    }
    
    return sharedInstance;
}

+ (BOOL)isNetworkAvaiable {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable) {
        UIAlertView *alert =[[UIAlertView alloc ] initWithTitle:@"No internet connection"
                                                        message:@"Please check connection"
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
        [alert show];
        return NO;
    }
    
    return YES;
}

+ (void)selfDestruction {
    sharedInstance = nil;
}

#pragma mark instance init methods

- (instancetype)initWithLogin:(NSString *)companyLogin {
    self = [super init];
    if (self) {
//        self.cache = nil;
        self.companyLogin = [companyLogin copy];
        _connection = [[ACHJSONRPCConnection alloc] initWithCompanyLogin:self.companyLogin];
//        _connection = [ACHJSONRPCConnection sharedInstanceWithLogin:self.companyLogin];
        self.shedduleTimeDict = [[NSMutableDictionary alloc] init];
        self.cache = [[NSCache alloc]init];

    }
//    [self differentDataDictFromServer];
    return self;
}

#pragma mark get data methods

- (NSDictionary *)startTimeMatrixOfDay:(NSString *)requiredDay event:(NSString *)eventId perfomer:(NSString *)unitId {
    NSString *unit = unitId;
    if (!unitId) {
        unit = @"";
    }
    NSDictionary *getStartTimeMatrix = @{
                                            @"method" : @"getStartTimeMatrix",
                                            @"params" : @[requiredDay, requiredDay, eventId, unit]
                                        };
    
    NSMutableDictionary *shedduleTime = [[NSMutableDictionary alloc] init];
    NSDictionary *startTimeTemp = [_connection requestWithDict:getStartTimeMatrix fromURL:registration];
    if (startTimeTemp) {
        NSArray *startTimeData = [[NSArray alloc]initWithArray:startTimeTemp[@"result"][requiredDay]];
        for (NSString *time in startTimeData) {
            NSString *truncatedString = [time substringToIndex:[time length] - 3];
//            [shedduleTime setValue:@"available" forKey:truncatedString];
            BOOL isTimePluginSwitch = [self.companyInfo[@"show_in_client_timezone"] boolValue];
//            NSLog(@"show_in_client_timezone: %hhd", isTimePluginSwitch);
            if (isTimePluginSwitch) {
                NSString *convertedTime = [self convertHoursFromServiceHours:truncatedString];
//                NSLog(@"original time %@", truncatedString);
//                NSLog(@"converted time %@", convertedTime);
                [shedduleTime setValue:@"available" forKey:convertedTime];
            } else {
                [shedduleTime setValue:@"available" forKey:truncatedString];
            }
        }
//        NSLog(@"startTimeData %@", startTimeData);
//        NSLog(@"self.companyInfo %@", self.companyInfo[@"timezone"]);
        
    }
    [self timeMatrixOfDay:[shedduleTime copy]];
    return [shedduleTime copy];
}

- (NSString *)convertHoursFromServiceHours:(NSString *)serviceHours {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    NSDate *date = [dateFormat dateFromString:serviceHours];
    NSTimeZone *serviceTime = [NSTimeZone timeZoneWithName:self.companyInfo[@"timezone"]];
    NSTimeZone *localTime = [NSTimeZone localTimeZone];
    NSDate *localDateNow = [NSDate date];
    NSInteger sourceGMTOffset = [localTime secondsFromGMTForDate:localDateNow];
    NSInteger destinationGMTOffset = [serviceTime secondsFromGMTForDate:localDateNow];
    NSTimeInterval interval =  sourceGMTOffset - destinationGMTOffset;
    NSDate *dateTimeHoursAhead = [date dateByAddingTimeInterval:interval];
    NSString *convertedTime = [dateFormat stringFromDate:dateTimeHoursAhead];
//    NSLog(@"original time: %@", serviceHours);
//    NSLog(@"convert  time: %@", convertedTime);
    return convertedTime;
}


- (NSString *)convertHoursToLocalHours:(NSString *)serviceHours {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm"];
    NSDate *date = [dateFormat dateFromString:serviceHours];
    NSTimeZone *serviceTime = [NSTimeZone timeZoneWithName:self.companyInfo[@"timezone"]];
    NSTimeZone *localTime = [NSTimeZone localTimeZone];
    NSDate *localDateNow = [NSDate date];
    NSInteger sourceGMTOffset = [localTime secondsFromGMTForDate:localDateNow];
    NSInteger destinationGMTOffset = [serviceTime secondsFromGMTForDate:localDateNow];
    NSTimeInterval interval =  destinationGMTOffset - sourceGMTOffset;
    NSDate *dateTimeHoursAhead = [date dateByAddingTimeInterval:interval];
    NSString *convertedTime = [dateFormat stringFromDate:dateTimeHoursAhead];

//    NSLog(@"original time: %@", serviceHours);
//    NSLog(@"convert  time: %@", convertedTime);
    return convertedTime;
}


- (NSDictionary *)timeMatrixOfDay:(NSDictionary *)dict {
    return dict;
}

- (NSDictionary *)bookWithEvent:(NSString *)eventId
               unitId:(NSString *)unitId
          requiredDay:(NSString *)requiredDay
         requiredTime:(NSString *)requiredTime
          userDetails:(NSDictionary *)userDetails
     additionalFields:(NSDictionary *)additionalFields {
    
//    NSLog(@"userDetails: %@", userDetails);
    
    BOOL isTimePluginSwitch = [self.companyInfo[@"show_in_client_timezone"] boolValue];
    if (isTimePluginSwitch) {
        NSString *convertedTime = [self convertHoursToLocalHours:requiredTime];
        requiredTime = [convertedTime copy];

    }
    NSDictionary *book = @{
                           @"method" : @"book",
                           @"params" : @[eventId,
                                         unitId,
                                         requiredDay,
                                         requiredTime,
                                         userDetails,
                                         additionalFields,
                                         ]
                            };
//    NSLog(@"booking: %@", book);
    NSDictionary *bookResult = [_connection requestWithDict:book fromURL:registration];
//    NSLog(@"bookResult %@", bookResult);
    if (bookResult[@"error"]) {
        UIAlertView * alert = [[UIAlertView alloc ] initWithTitle:@"Server error"
                                                          message:bookResult[@"error"][@"message"]
                                                         delegate:self
                                                cancelButtonTitle:@"Ok"
                                                otherButtonTitles: nil];
        [alert show];
        return nil;
    }
    
    if (debugTools) {
        NSLog(@"book %@", book);
        NSLog(@"bookResult %@", bookResult);
    }

    return bookResult;
}


- (BOOL)isPluginActivated:(NSString *)pluginName {
    NSString *pluginNameInCache = [self.cache objectForKey:pluginName];
    
    if(!pluginNameInCache) {
        NSDictionary *requestDict = @{
                                        @"method" : @"isPluginActivated",
                                        @"params" : @[pluginName]
                                    };
        NSDictionary *tempResultDict = [_connection requestWithDict:requestDict fromURL:registration];
        if (tempResultDict){
            [self.cache setObject:tempResultDict[@"result"] forKey:pluginName];
        }

        pluginNameInCache = tempResultDict[@"result"];
    }
    
    BOOL isActivated = [pluginNameInCache boolValue];
//    NSLog(@"plugin is Activated: %@", tempResultDict[@"result"]);

    return isActivated;

}

- (BOOL)isPaymentRequired:(NSString *)eventId {
    
    NSString *eventIdent = [NSString stringWithFormat:@"eventId%@",eventId];
    NSString *eventIdNameInCache = [self.cache objectForKey:eventIdent];
    
    if(!eventIdNameInCache) {
        NSDictionary *requestDict = @{
                                      @"method" : @"isPaymentRequired",
                                      @"params" : @[eventId]
                                      };
        NSDictionary *tempResultDict = [_connection requestWithDict:requestDict fromURL:registration];
        if (tempResultDict){
            [self.cache setObject:tempResultDict[@"result"] forKey:eventIdent];
        }
        
        eventIdNameInCache = tempResultDict[@"result"];
    }
    
    
    BOOL isPaymentRequired = [eventIdNameInCache boolValue];
//    NSLog(isPaymentRequired ? @"Yes" : @"No");
//    NSLog(@"isActivated: %d", StringFromBoolean(isActivated));
    
    return isPaymentRequired;
    
}

- (NSArray *)getCategoryList {
    NSMutableArray *category = [self.cache objectForKey:@"categoryList"];
    if (!category) {
        category = [[NSMutableArray alloc] init];
        NSDictionary *getCategoriesList = @{
                                                @"method" : @"getCategoriesList"
                                            };
        NSDictionary *tempCategoryList = [_connection requestWithDict:getCategoriesList fromURL:registration];
        NSDictionary *categoryKeysList = tempCategoryList[@"result"];
        for (NSString *item in categoryKeysList) {
            [category addObject:categoryKeysList[item]];
        }
        if (category)
            [self.cache setObject:category forKey:@"categoryList"];
    }
    
    return [category copy];
}

- (NSDictionary *)getEventList {
    NSDictionary *resultEventList = [self.cache objectForKey:@"eventList"];
    if (!resultEventList) {
        NSDictionary *eventList = @{@"method" : @"getEventList"};
        NSDictionary *tempEventList = [_connection requestWithDict:eventList fromURL:registration];
        resultEventList = [tempEventList[@"result"] copy];
        if (resultEventList)
            [self.cache setObject:resultEventList forKey:@"eventList"];
    }
    return resultEventList;
}

- (NSDictionary *)getPerformersList {
    NSDictionary *performersList = [self.cache objectForKey:@"performersList"];
    if (!performersList) {
        NSDictionary *unitList = @{@"method" : @"getUnitList"};
        NSDictionary *tempPerformersList = [_connection requestWithDict:unitList fromURL:registration];
        performersList = [tempPerformersList[@"result"] copy];
        if (performersList)
            [self.cache setObject:performersList forKey:@"performersList"];
    }
    
    return [performersList copy];
}

- (NSDictionary *)getAnyUnitData {
    NSDictionary *unitDataList = [self.cache objectForKey:@"getAnyUnitData"];
    if (!unitDataList) {
        NSDictionary *unitList = @{@"method" : @"getAnyUnitData"};
        NSDictionary *tempPerformersList = [_connection requestWithDict:unitList fromURL:registration];
        unitDataList = [tempPerformersList[@"result"] copy];
        if (unitDataList)
            [self.cache setObject:unitDataList forKey:@"getAnyUnitData"];
    }
    
    return [unitDataList copy];
}

- (NSArray *)availableUnitsForEvent:(NSString *)eventId
                               date:(NSString *)date
                               time:(NSString *)time
                              count:(NSString *)count {
    
    BOOL isAnyUnitPluginSwitch = [self isPluginActivated:@"any_unit"];
    NSDictionary *bookResult;
    
    BOOL isTimePluginSwitch = [self.companyInfo[@"show_in_client_timezone"] boolValue];
    if (isTimePluginSwitch) {
        NSString *convertedTime = [self convertHoursToLocalHours:time];
        time = [convertedTime copy];
//        NSLog(@"convertedTime: %@", time);
    }

    NSString *reservationTime = [NSString stringWithFormat:@"%@ %@", date, time];

    
    if (isAnyUnitPluginSwitch) {
        NSDictionary *requestList = @{
                                      @"method" : @"getAvailableUnits",
                                      @"params" : @[eventId,
                                                    reservationTime,
                                                    count]
                                      };
        bookResult = [_connection requestWithDict:requestList fromURL:registration];
        if (debugTools) {
            NSLog(@"book %@", requestList);
            NSLog(@"bookResult %@", bookResult);
        }
        if (bookResult[@"error"]) {
            UIAlertView * alert = [[UIAlertView alloc ] initWithTitle:@"Server error"
                                                              message:bookResult[@"error"][@"message"]
                                                             delegate:self
                                                    cancelButtonTitle:@"Ok"
                                                    otherButtonTitles: nil];
            [alert show];
            return nil;
        }
//        NSLog(@"book %@", requestList);
//        NSLog(@"bookResult %@", bookResult);

    }
    
    return [bookResult[@"result"] copy];
}


- (UIImage *)pictureFormURL:(NSString *)stringURL {
    UIImage *image = [self.cache objectForKey:stringURL];
//    NSLog(@"stringURL %@", stringURL);
    if(!image) {
        NSString *imageURL = [NSString stringWithFormat:@"http://simplybook.me%@", stringURL];
        NSURL *fullURL = [NSURL URLWithString:imageURL];
        NSData *data = [NSData dataWithContentsOfURL:fullURL];
        image = [self pictureResize:data];
        [self.cache setObject:image forKey:stringURL];
    }
    
    return image;
}

- (UIImage *)pictureResize:(NSData *)data {
    UIImage *image = [UIImage imageWithData:data];
    CGSize itemSize = CGSizeMake(80, 80);
    UIGraphicsBeginImageContext(itemSize);
    CGRect imageRect;
    if (image.size.height > image.size.width) {
        double copressRate = image.size.width/image.size.height;
        double xCoord = (itemSize.width * (1 - copressRate)) / 2;
        imageRect = CGRectMake(xCoord, 0.0, itemSize.width * copressRate, itemSize.height);
    } else if (image.size.height < image.size.width) {
        double copressRate = image.size.height/image.size.width;
        double yCoord = (itemSize.height * (1 - copressRate)) / 2;
        imageRect = CGRectMake(0.0, yCoord, itemSize.width, itemSize.height * copressRate);
    } else {
        imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
    }
    [image drawInRect:imageRect];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (NSArray *)additionalFields:(NSString *)eventId {
    NSDictionary *getAdditionalFields = @{
                                          @"method" : @"getAdditionalFields",
                                          @"params" : @[eventId]
                                          };
    NSDictionary *resultAdditionalFields = [_connection requestWithDict:getAdditionalFields fromURL:registration];
    
    return [resultAdditionalFields[@"result"] copy];
}

- (NSDictionary *)getCompanyInfo {
    self.companyInfo = nil;
    NSDictionary *getTimeframe = @{@"method" : @"getCompanyInfo"};
    NSDictionary *result = [_connection requestWithDict:getTimeframe fromURL:registration];
    
    self.companyInfo = [result[@"result"] copy];
    return [result[@"result"] copy];
}

- (NSDictionary *)cancelBookingID:(NSString *)bookingId sign:(NSString *)sign {
    NSDictionary *cancelBooking = @{
                                    @"method" : @"cancelBooking",
                                   @"params" : @[bookingId, sign]
                                };
    NSDictionary *result = [_connection requestWithDict:cancelBooking fromURL:registration];
    
    return [result[@"result"] copy];
}

- (NSDictionary *)companyiesWithContainsWord:(NSString *)containsWord
                            seachingRadius:(int)radius
                           currentLatitude:(double)latitude
                          currentLongitude:(double)longitude
                                  startRow:(int)startRow
                               requestRows:(int)requestRows {
    if (containsWord) {
        NSDictionary *requestDictionary = @{
                                            @"search_string" : containsWord,
                                            @"service_name" : @"",
                                            @"company_name" : @"",
                                            @"company_address" : @"",
                                            @"tag_ids" : @[],
                                            @"tags" : @"",
                                            @"country_id" : @"",
                                            @"city_id" : @"",
                                            @"nearby" : @{
                                                        @"radius" : [NSNumber numberWithInt:radius],
                                                        @"center" : @{
                                                                    @"lat" : [NSNumber numberWithDouble:latitude],
                                                                    @"lng" : [NSNumber numberWithDouble:longitude]
                                                                    }
                                                            }
                                            };
        
        NSDictionary *companyList = @{
                                        @"method" : @"getCompanyList",
                                        @"params" : @[requestDictionary,
                                                      [NSNumber numberWithInt:startRow],
                                                      [NSNumber numberWithInt:requestRows]
                                                      ]
                                    };
    //    NSLog(@"request %@",companyList);
        NSDictionary *result = [_connection requestWithDict:companyList fromURL:catalog];
        return result[@"result"];
    }
    
    return nil;
}


- (void)showProgressHUD {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [ProgressHUD show:@"Loading..."];
    });
}

- (void)hideProgressHUD {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [ProgressHUD dismiss];
    });
}


#pragma mark MD5 function

- (NSString *)md5:(NSString *)input {
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}


- (NSString *)singGeneratingWithToken:(NSString *)deviceToken {
    NSString *sign = [NSString stringWithFormat:@"%@%@",
                                                deviceToken,
                                                secureKeyAPI
                                            ];
    return [self md5:sign];
}

- (NSDictionary *)bookingInfoWithID:(NSString *)bookingID bookingHash:(NSString *)bookingHash {
    
    NSString *sign = [NSString stringWithFormat:@"%@%@%@",
                      bookingID,
                      bookingHash,
                      secureKeyAPI
                      ];
    NSString *md5Sign = [self md5:sign];

//    NSLog(@"bookingID %@ %@ %@", bookingID,bookingHash,secureKeyAPI );
    NSDictionary *bookingList = @{
                                  @"method" : @"getBookingDetails",
                                  @"params" : @[bookingID,
                                                md5Sign
                                                ]
                                  };
//    NSLog(@"bookingList %@", bookingList);
    NSDictionary *result = [_connection requestWithDict:bookingList fromURL:registration];

//    NSLog(@"result %@", result);
    return [result[@"result"] copy];
}

- (void)setAllowNotificationWithToken:(NSString *)deviceToken
                           deviceType:(NSString *)deviceType
                         companyLogin:(NSString *)companyLogin
                   allowNotifications:(NSString *)isAllow {
    NSString *sign = [self singGeneratingWithToken:deviceToken];
//    NSLog(@"sign %@", sign);

    if (deviceToken) {
        NSDictionary *companyList = @{
                              @"method" : @"setAllowNotification",
                              @"params" : @[deviceToken,
                                            deviceType,
                                            companyLogin,
                                            isAllow,
                                            sign]
                              };
        [_connection requestWithDict:companyList fromURL:user];
//        NSDictionary *result = [_connection requestWithDict:companyList fromURL:user];
//        NSLog(@"companyList %@",companyList);
    }
}



- (void)differentDataDictFromServer {
    
    
//    getCompanyInfo ()
    
//    NSDictionary *getTimeframe = @{@"method" : @"getCompanyInfo"};
//    NSDictionary *result = [_connection requestWithDict:getTimeframe];
//    NSLog(@"result of request %@", result);

//    cancelBooking ()
    
//    NSString *bookingId = @"563";
//    NSString *sign = @"8d494a8ee089ac7594a6551b734180b9";
//    
//    NSDictionary *cancelBooking = @{
//                                    @"method" : @"cancelBooking",
//                                    @"params" : @[bookingId, sign]
//                                    };
//    NSDictionary *result = [_connection requestWithDict:cancelBooking fromURL:registration];
//    NSLog(@"result of cancelBooking %@", result);

    
//    NSDictionary *getTimeframe = @{@"method" : @"getTimeframe"};
//    NSDictionary *result = [_connection requestWithDict:getTimeframe];
//    NSLog(@"result of request %@", result);

//    NSDictionary *getEventList = @{@"method" : @"getEventList"};
//    NSDictionary *evenList = [_connection requestWithDict:getEventList];
//    NSLog(@"result of request %@", evenList);

//    NSDictionary *getUnitList = @{@"method" : @"getUnitList"};
//    NSDictionary *result = [_connection requestWithDict:getUnitList];
//    NSLog(@"result of request %@", result);

    
//    NSDictionary *getCategoriesList = @{@"method" : @"getCategoriesList"};
//    NSDictionary *categoryList = [_connection requestWithDict:getCategoriesList];
//    NSLog(@"list of category: %@", categoryList[@"result"]);
    
//    getAdditionalFields ($eventId)
//
//    NSDictionary *getAdditionalFields = @{
//                                            @"method" : @"getAdditionalFields",
//                                            @"params" : @[@(15)]
//                                          };
//    NSDictionary *result = [_connection requestWithDict:getAdditionalFields];
//    NSLog(@"result of request %@", result);
//
//    NSDictionary *getAdditionalFields2 = @{
//                                          @"method" : @"getAdditionalFields",
//                                          @"params" : @[@(16)]
//                                          };
//    NSDictionary *result2 = [_connection requestWithDict:getAdditionalFields2];
//    NSLog(@"result of request %@", result2);
    
    
    
//    NSDictionary *calculateEndTime = @{
//                                      @"method" : @"calculateEndTime",
//                                      @"params" : @[@"2014-09-04", @(1), @(1)]
//                                      };
//    NSDictionary *result = [_connection requestWithDict:calculateEndTime];
//    NSLog(@"result of request %@", result);

    
  
//    NSDictionary *getReservedTime = @{
//                                          @"method" : @"getReservedTime",
//                                          @"params" : @[@"2014-09-01", @"2014-09-01", @(1), @(1)]
//                                      };
//    
//    NSDictionary *reservedTime = [_connection requestWithDict:getReservedTime];
//    NSLog(@"result of request %@", reservedTime[@"result"]);

//    NSDictionary *startTimeTemp = [_connection requestWithDict:getStartTimeMatrix fromURL:registration];
//
//    [self initWithLogin:@"dev"];
    NSDictionary *getStartTimeMatrix = @{
                                         @"method" : @"getStartTimeMatrix",
                                         @"params" : @[@"2014-12-02", @"2014-12-02", @"3", @"null"]
                                         };
    NSDictionary *timeMatrix = [_connection requestWithDict:getStartTimeMatrix fromURL:registration];
    NSLog(@"result of request %@", timeMatrix[@"result"]);



//    NSDictionary *getWorkDaysInfo  = @{
//                                       @"method" : @"getWorkDaysInfo",
//                                       @"params" : @[@"2014-08-29", @"2014-09-01", @(1)]
//                                       };
//    [self createRequestWithDict:getWorkDaysInfo];


//    getAdditionalFields ($eventId)

//    NSDictionary *getAdditionalFields = @{
//                                          @"method" : @"getAdditionalFields",
//                                          @"params" : @[@(15)]
//                                          };
//    NSDictionary *resultAdditionalFields = [_connection requestWithDict:getAdditionalFields];
//    NSLog(@"result of request %@", resultAdditionalFields[@"result"]);

//Client bookinkg
//    NSDictionary *book  = @{
//                            @"method" : @"book",
//                            @"params" : @[@(15), @(1), @"2014-12-30", @"12:00:00",
//                                        @{@"name": @"Dad", @"email": @"plrs@mail.ru", @"phone": @"+380504191155"},
//                                          @{@"e8d0e2482346606637191aa55eb29a43" : @"smoking", @"a670bec222ecf216eac2cf4768e391fa" : @"black" }]
//                            };
//    NSDictionary *booking = [_connection requestWithDict:book];
//    NSLog(@"result of request %@", booking);
//

//Confirm bookinkg
//    NSDictionary *confirmBooking   = @{
//                                     @"method" : @"confirmBooking",
//                                     @"params" : @[@"uos4v4o"]
//                                     };
//    [self createRequestWithDict:confirmBooking ];
//

//Cancel bookinkg
//    NSDictionary *cancelBooking  = @{
//                                       @"method" : @"cancelBooking",
//                                       @"params" : @[@(1), @"uos4v4o"]
//                                       };
//    [self createRequestWithDict:cancelBooking];
    
    
 }

@end
