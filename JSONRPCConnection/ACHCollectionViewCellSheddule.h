//
//  ACHCollectionViewCell.h
//  JSONRPCConnection
//
//  Created by Капитан on 04.09.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACHCollectionViewCellSheddule : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@property (nonatomic, assign) BOOL enable;

@end
