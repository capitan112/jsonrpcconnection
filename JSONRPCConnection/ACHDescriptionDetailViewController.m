//
//  ACHDescriptionDetailViewController.m
//  SimplyBook-me
//
//  Created by Капитан on 03.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHDescriptionDetailViewController.h"
#import "ACHTableViewControllerService.h"
#import "ACHDataSource.h"

static NSString *const descriptionServiceSegue = @"descriptionServiceSegue";

@interface ACHDescriptionDetailViewController ()

@property (weak, nonatomic) IBOutlet UITextView *descriptionView;
@property (nonatomic, strong) ACHTableViewControllerService *destinationController;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ACHDescriptionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = self.allCategory[@"name"];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    if (![self.allCategory[@"description"] isEqual:[NSNull null]]) {
        self.descriptionView.text = self.allCategory[@"description"];
    } else {
        self.descriptionView.text = @"";
    }
}

- (IBAction)selectedButton:(id)sender {
    BOOL isNetwork = [ACHDataSource isNetworkAvaiable];
    if (isNetwork) {
        [self performSegueWithIdentifier:descriptionServiceSegue sender:self];
        self.destinationController.sendedListOfEvents = self.sendedListOfEvents;
//        self.destinationController.companyInfo = [self.companyInfo copy];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:descriptionServiceSegue]) {
        self.destinationController = [segue destinationViewController];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
