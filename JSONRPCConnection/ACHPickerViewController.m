//
//  ACHAddFieldsViewController.m
//  SimplyBook-me
//
//  Created by Капитан on 09.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ACHPickerViewController.h"

@interface ACHPickerViewController ()

@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (nonatomic, strong) NSArray *valueOfPicker;
@property (nonatomic, strong) NSString *keyOfField;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@end

@implementation ACHPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (![self.dataPickerSource[@"values"] isEqual: [NSNull null]]) {
        self.valueOfPicker = [self.dataPickerSource[@"values"] componentsSeparatedByString:@","];
        self.keyOfField = self.dataPickerSource[@"name"];
        if (self.selectedValue) {
            NSInteger index = [self.valueOfPicker indexOfObject:self.selectedValue];
            [self.picker reloadAllComponents];
            [self.picker selectRow:index inComponent:0 animated:YES];
        }
    }
    self.confirmButton.layer.cornerRadius = 5;
    self.confirmButton.layer.borderWidth = 1;
    self.confirmButton.layer.borderColor = self.confirmButton.tintColor.CGColor;
    self.navigationItem.hidesBackButton = YES;
}

//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:NO];
//    [self.delegate selectedValue:self.selectedValue forKey:self.keyOfField];
////    NSLog(@"self.selectedValue %@", self.selectedValue);
//}

#pragma mark - pickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.valueOfPicker count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    self.selectedValue = self.valueOfPicker[row];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.valueOfPicker[row];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)confirmButtonPressed:(id)sender {
    [self.delegate selectedValue:self.selectedValue forKey:self.keyOfField];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
