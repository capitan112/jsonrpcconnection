//
//  ACHdescriptionPerfomerVC.h
//  SimplyBook-me
//
//  Created by Капитан on 04.11.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACHdescriptionPerfomerVC : UIViewController

@property (nonatomic, strong) NSDictionary *fullPerformersList;
@property (nonatomic, strong) NSString *perfomerID;
@property (nonatomic, strong) NSString *eventID;
@property (nonatomic, strong) NSDictionary *currentEventsList;
@property (nonatomic, strong) NSString *currentPerfomerName;
//@property (nonatomic, strong) NSMutableDictionary *companyInfo;


@end
