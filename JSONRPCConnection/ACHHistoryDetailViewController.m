//
//  ACHHistoryDetailViewController.m
//  SimplyBook-me

//  Created by Капитан on 17.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.

#import "ACHHistoryDetailViewController.h"
#import <CommonCrypto/CommonDigest.h>
#import "ACHDataSource.h"
#import "Company.h"
//#import "ACHGlobalData.h"

static NSString *secureKeyAPI = @"top_secret_2349891071e405fcb73064bd4031ba2ff45099";
static NSString *const reuseCellIdentifier = @"fieldsCell";

@interface ACHHistoryDetailViewController ()

@property (nonatomic, strong) ACHDataSource *dataSource;
@property (nonatomic, strong) Company *company;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceLabel;
@property (weak, nonatomic) IBOutlet UILabel *perfomerLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBookingButton;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *additionalFieldsLabel;
@property (nonatomic, strong) NSDictionary *additionalFields;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ACHHistoryDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showBookingDetails];
    [ACHDataSource selfDestruction];
    self.dataSource = [ACHDataSource sharedInstanceWithLogin:self.selectedHistory.companyName.login];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self showCancelButton];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(backToTableView)
                                                 name:@"backToTableView"
                                               object:nil];
}

- (void)showCancelButton {
    NSDate *today = [NSDate date];
    NSDate *earlierDate = [today laterDate:self.selectedHistory.startDateTime];
    BOOL isExpired = [earlierDate isEqualToDate:today];
    
    self.cancelBookingButton.layer.cornerRadius = 5;
    self.cancelBookingButton.layer.borderWidth = 1;
    self.cancelBookingButton.layer.borderColor = [[UIColor redColor] CGColor];
    self.cancelBookingButton.hidden = isExpired;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)showBookingDetails {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateAndTimeBooking = [formatter stringFromDate:self.selectedHistory.startDateTime];
    self.companyNameLabel.text = self.selectedHistory.companyName.companyName;
    self.emailLabel.text = self.selectedHistory.companyName.email;
    self.phoneLabel.text = self.selectedHistory.companyName.phone;
    self.serviceLabel.text = self.selectedHistory.serviceName;
    self.perfomerLabel.text = self.selectedHistory.perfomerName;
    self.dateLabel.text = dateAndTimeBooking;
    self.additionalFields = [NSKeyedUnarchiver unarchiveObjectWithData:self.selectedHistory.additionalFields];
    if ([self.additionalFields count] == 0) {
        self.tableView.hidden = YES;
        self.additionalFieldsLabel.hidden = YES;
    }
}

#pragma mark Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.additionalFields count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *headerCell = [self.tableView dequeueReusableCellWithIdentifier:reuseCellIdentifier];
    if (headerCell == nil) {
        headerCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseCellIdentifier];
    }
    
    NSString *keyOfFields = [self.additionalFields allKeys][indexPath.row];
    NSString *valueOfFields = self.additionalFields[keyOfFields];
    headerCell.textLabel.text = keyOfFields;
    headerCell.detailTextLabel.text = valueOfFields;
    
    return headerCell;
}

#pragma mark MD5 function

- (NSString *)md5:(NSString *)input {
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}

- (NSString *)singGenerating {
     NSString *sign = [NSString stringWithFormat:@"%@%@%@",
                                                self.selectedHistory.bookingId,
                                                self.selectedHistory.bookingHash,
                                                secureKeyAPI
                                            ];
    return [self md5:sign];
}

#pragma Confirmation button

- (IBAction)canceledBooking:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Booking cancellation"
                                                    message:@"Do you want to cancel booking?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [_dataSource showProgressHUD];
            NSString *sign = [self singGenerating];
            NSString *bookingStringId = [NSString stringWithFormat:@"%@", self.selectedHistory.bookingId];
            NSDictionary *resultOfRequest = [self.dataSource cancelBookingID:bookingStringId sign:sign];
            if (resultOfRequest){
//                NSLog(@"cancel ready");
                [[NSNotificationCenter defaultCenter] postNotificationName:@"backToTableView" object:self];
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.dataSource hideProgressHUD];
                });
            }
        });
    }
}

- (void)backToTableView {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.dataSource hideProgressHUD];
        [self.delegate ACHHistoryDetailViewControllerDelete:self.selectedHistory];
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
