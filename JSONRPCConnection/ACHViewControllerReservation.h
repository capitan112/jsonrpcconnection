//
//  ACHViewControllerReservation.h
//  SimplyBook-me
//
//  Created by Капитан on 08.10.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACHUserDetailsViewController.h"
#import "SetSelectedData.h"

@interface ACHViewControllerReservation : UITableViewController <UserDetail, SetSelectedData>

@property (nonatomic, strong) NSMutableDictionary *dataReservation;
@property (nonatomic, strong) NSMutableDictionary *userDetails;
@property (nonatomic, strong) NSDictionary *fullPerformersList;

@end
